﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using ODSClient.Model;
using ODSClient.Model.Data;
using ODSClient.Model.Screen;
using ODSClient.Model.Screen.Controls;
using ODSClient.Service.Interview;


namespace Oracle.Examples.Web.Application.Controllers
{
    public class InvestigateController : ApplicationController
    {
        public ActionResult Investigate()
        {
            ActionResult result = CheckSession();
            if (result != null)
                return result;
            try
            {
                ViewData["investigate_flag"] = "true";
                Investigate(this.Request.Params["goalstate"]);
                ActionResult view = NextView(GetSessionData().CurrentScreen);
                if (view != null)
                    return view;
            }
            catch (Exception ex)
            {
                ViewData["error_message"] = ex.Message;
                return View("Error");
            }
            return View();
        }



        public ActionResult AddInstance()
        {
            ActionResult result = CheckSession();
            if (result != null)
                return result;

            SessionData sessionData = GetSessionData();
            ClientInterviewScreen currentScreen = sessionData.CurrentScreen;
            GetFormData(currentScreen);
            AddNewInstance((ContainmentControl)currentScreen.ScreenControls[0]);
           
            ViewData["screen"] = currentScreen;
            ViewData["message"] = "Add";
            
            return View("InvestigateContainment");
        }

        public ActionResult DeleteInstance()
        {
            ActionResult result = CheckSession();
            if (result != null)
                return result;

            SessionData sessionData = GetSessionData();
            ClientInterviewScreen currentScreen = sessionData.CurrentScreen;
            GetFormData(currentScreen);

            String removeInstances = this.Request.Form["remove"];
            if (removeInstances != null)
            {
                String[] indexString = removeInstances.Split(',');
                if (indexString.Length == 0)
                {
                    ViewData["message"] = "No instance to delete";
                }
                else
                {

                    List<ScreenControl> ctrls = ((ContainmentControl)currentScreen.ScreenControls[0]).Controls;

                    for (int i = 0; i < indexString.Length; i++)
                    {
                        int index = int.Parse(indexString[i]);
                        ctrls.RemoveAt(index);
                    }
                }
            }

            ViewData["screen"] = sessionData.CurrentScreen;
            return View("InvestigateContainment");
        }

        public ActionResult Submit()
        {
            ActionResult result = CheckSession();
            if (result != null)
                return result;
            String investigateFlag = Request.Form["investigate_flag"];
            ViewData["investigate_flag"] = investigateFlag;
            if (this.Request.Form["submit_button"].Equals("Add Instance")) {
                return AddInstance();
            }
            else if (this.Request.Form["submit_button"].Equals("Remove Instance/s"))
            {
                return DeleteInstance();
            }

            try
            {
                SessionData sessionData = GetSessionData();
                GetFormData(sessionData.CurrentScreen);
                if (investigateFlag.Equals("true"))
                {
                    Investigate(null);
                    ActionResult view = NextView(sessionData.CurrentScreen);
                    if (view != null)
                        return view;
                }
                else
                {
                    //call setscreen
                    //redirect to Summary
                    if(SetScreen())
                    {
                        //redirect to user set data if ok
                        GetSessionData().CurrentScreen = null;
                        return Redirect("/Interview/Summary");
                    }
                    
                }
            }
            catch (Exception ex)
            {
                ViewData["error_message"] = ex.Message;
                return View("Error");
            }
            return View("Investigate");
        }

        public ActionResult GetScreen()
        {
            ActionResult result = CheckSession();
            if (result != null)
                return result;

            try
            {
                SessionData sessionData = GetSessionData();
                String screenId = Request.Params["screenid"];
                InterviewService serviceProxy = ServiceLocator.GetServiceProxy(sessionData.RulebaseName);
                ClientInterviewScreen currentScreen = serviceProxy.GetScreen(sessionData.SessionId, screenId);
                sessionData.CurrentScreen = currentScreen;
                ViewData["investigate_flag"] = "false";
                ViewData["screen"] = currentScreen;
                ActionResult view = NextView(currentScreen);
                if (view != null)
                    return view;
            }
            catch (Exception ex)
            {
                ViewData["error_message"] = ex.Message;
                return View("Error");
            }
            return View("Investigate");
        }

        public override bool IsSessionRequired()
        {
            return true;
        }


        #region private methods
        private bool SetScreen()
        {
            SessionData sessionData = GetSessionData();
            bool success = true;
            InterviewService serviceProxy = ServiceLocator.GetServiceProxy(sessionData.RulebaseName);
            sessionData.CurrentScreen = serviceProxy.SetScreen(sessionData, out success);
            ViewData["screen"] = sessionData.CurrentScreen;
            return success;
        }

        private void Investigate(String goalState)
        {
            SessionData sessionData = GetSessionData();
            if (goalState != null)
                sessionData.CurrentGoalState = goalState;
            InterviewService serviceProxy = ServiceLocator.GetServiceProxy(sessionData.RulebaseName);
            serviceProxy.Investigate(sessionData);
            ViewData["screen"] = sessionData.CurrentScreen;
        }

        private void AddNewInstance(ContainmentControl mainCtrl)
        {
            List<ScreenControl> instances = mainCtrl.Controls;
            int currentSize = instances.Count;

            ScreenEntityInstanceControl template = (ScreenEntityInstanceControl)instances[0];
            if (!template.ControlId.Equals("--BLANK--"))
            {
                currentSize++;
            }
            ScreenEntityInstanceControl newInstance = new ScreenEntityInstanceControl();
            newInstance.ControlId = "[" + currentSize + "]";
            newInstance.EntityId = template.EntityId;
            //copy controls
            List<ScreenControl> templateInputCtrls = template.Controls;
            List<ScreenControl> inputCtrls = new List<ScreenControl>(templateInputCtrls.Count);
            templateInputCtrls.ForEach(delegate(ScreenControl ctrl)
            {
                InputScreenControl templateInputCtrl = (InputScreenControl)ctrl;
                InputScreenControl inputCtrl = new InputScreenControl(templateInputCtrl.GetControlType());
                inputCtrl.ControlId = templateInputCtrl.ControlId;
                inputCtrl.Caption = templateInputCtrl.Caption;
                inputCtrl.Visible = templateInputCtrl.Visible;
                inputCtrl.Mandatory = templateInputCtrl.Mandatory;
                inputCtrl.Options = templateInputCtrl.Options;
                inputCtrl.DefaultValue = templateInputCtrl.DefaultValue;
                inputCtrl.Value = new Unknown();
                inputCtrls.Add(inputCtrl);
            });
            newInstance.Controls = inputCtrls;
            instances.Add(newInstance);
        }

        private ActionResult NextView(ClientInterviewScreen currentScreen)
        {
            if (currentScreen != null)
            {
                if (currentScreen.ScreenType.Equals("summary"))
                {
                    GetSessionData().CurrentScreen = null;
                    return View("Summary");
                }
                else if (currentScreen.ScreenControls != null)
                {
                    List<ScreenControl> controls = currentScreen.ScreenControls;
                    if (controls[0] is ContainmentControl)
                    {
                        return View("InvestigateContainment");
                    }
                }
            }
            return null;
        }
        #endregion
    }
}
