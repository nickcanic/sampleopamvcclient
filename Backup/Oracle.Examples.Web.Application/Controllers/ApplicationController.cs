﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ODSClient.Model;
using ODSClient.Model.Data;
using ODSClient.Model.Screen;
using ODSClient.Model.Screen.Controls;
using ODSClient.Service.Interview;
using Oracle.Examples.Web.Application.Utils;

namespace Oracle.Examples.Web.Application.Controllers
{
    public abstract class ApplicationController : Controller
    {

        abstract public bool IsSessionRequired();

        protected bool IsSessionFound()
        {
            return (this.HttpContext.Session[ApplicationConstants.SESSION_DATA] != null);
        }

        protected void SetSessionData(SessionData sessionData)
        {
            this.HttpContext.Session[ApplicationConstants.SESSION_DATA] = sessionData;
        }

        protected SessionData GetSessionData()
        {
            return (SessionData)this.HttpContext.Session[ApplicationConstants.SESSION_DATA];
        }

        protected ActionResult CheckSession()
        {
            if (IsSessionRequired() && !IsSessionFound())
            {
                ViewData["error_message"]="No Session Found.";
                return View("Error");
            }
            return null;
        }

        protected void ClearSession()
        {
            SessionData sessionData = GetSessionData();
            InterviewService serviceProxy = ServiceLocator.GetServiceProxy(sessionData.RulebaseName);
            serviceProxy.CloseSession(sessionData.SessionId);
            SetSessionData(null);
        }

        protected void GetFormData(ClientInterviewScreen screen)
        {
            screen.ScreenControls.ForEach(delegate(ScreenControl control)
            {
                if (control is ContainmentControl)
                {
                    GetFormDataForEntityCollect((ContainmentControl)control);
                }
                else
                {
                    SetFormValue(control, control.ControlId);
                }
            });

        }

        private void GetFormDataForEntityCollect(ContainmentControl mainCtrl)
        {
            List<ScreenControl> instances = mainCtrl.Controls;
            instances.ForEach(delegate(ScreenControl ctrl)
            {
                ScreenEntityInstanceControl ieCtrl = (ScreenEntityInstanceControl)ctrl;
                String instanceId = ieCtrl.ControlId;
                if (!instanceId.Equals("--BLANK--"))
                {
                    List<ScreenControl> ctrls = ieCtrl.Controls;
                    ctrls.ForEach(delegate(ScreenControl ieInputCtrl)
                    {
                        SetFormValue(ieInputCtrl, ieInputCtrl.ControlId + instanceId);
                    });
                }

            });
        }

        private void SetFormValue(ScreenControl ctrl, String ctrlId)
        {
            if (ctrl is InputScreenControl)
            {
                InputScreenControl inputCtrl = (InputScreenControl)ctrl;
                if (inputCtrl.Options == null)
                {
                    if (inputCtrl.Visible)
                    {

                        Object formValue = Request.Form[ctrlId];
                        inputCtrl.Value = (formValue == null || formValue.Equals("")) ? new Uncertain() : formValue;
                    }
                }
                else
                {
                    inputCtrl.Value = (String)GetSelectedOptionValue(Request.Form[ctrlId], inputCtrl.Options);
                }
            }
            else if (ctrl is ReferenceControl)
            {
                ((ReferenceControl)ctrl).TargetEntityInstanceId =
                    (String)GetSelectedOptionValue(Request.Form[ctrlId], ((ReferenceControl)ctrl).PossibleTarget);
            }
        }

        private Object GetSelectedOptionValue(String displayText, List<ListOption> options)
        {
            Object selectedValue = null;
            if (options != null)
            {
                for (int i = 0; i < options.Count; i++)
                {
                    ListOption option = options[i];
                    if (option.GetValue().Equals(displayText))
                    {
                        selectedValue = option.GetValue();
                        break;
                    }
                }
            }
            return selectedValue;

        }
    }
}
