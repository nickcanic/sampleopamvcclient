﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ODSClient.Model;
using ODSClient.Service.Interview;
using Oracle.Examples.Web.Application.Utils;

namespace Oracle.Examples.Web.Application.Controllers
{
    public class DataController : ApplicationController
    {
        public override bool IsSessionRequired()
        {
            return true;
        }

        public ActionResult LoadData()
        {
            try
            {
                if (this.Request.Form["submit_button"].Equals("OK"))
                {
                    String caseId = Request.Form["caseId"];
                    SessionData sessionData = GetSessionData();
                    InterviewService serviceProxy = ServiceLocator.GetServiceProxy(sessionData.RulebaseName);

                    String[] errors = serviceProxy.LoadCase(sessionData.SessionId, caseId);
                    sessionData.CurrentScreen = null;
                    if (errors == null || errors.Length == 0)
                    {
                        sessionData.CaseId = caseId;
                    }
                    else
                    {
                        ViewData["errors"] = errors;
                        return View("Error");
                    }
                }
            }
            catch (Exception ex)
            {
                ViewData["error_message"] = ex.Message;
                return View("Error");
            }

            return Redirect("/Interview/Summary");
        }

        public ActionResult ListCases()
        {
            try
            {
                SessionData sessionData = GetSessionData();
                InterviewService serviceProxy = ServiceLocator.GetServiceProxy(sessionData.RulebaseName);

                String[] caseIds = serviceProxy.ListCases(sessionData.SessionId);
                sessionData.CurrentScreen = null;
                ViewData["case_id"] = caseIds;
            }
            catch (Exception ex)
            {
                ViewData["error_message"] = ex.Message;
                return View("Error");
            }

            return View();
        }

        public ActionResult SaveData()
        {
            try
            {
                SessionData sessionData = GetSessionData();
                sessionData.CurrentScreen = null;
                ViewData["currentCaseId"] = sessionData.CaseId;
            }
            catch (Exception ex)
            {
                ViewData["error_message"] = ex.Message;
                return View("Error");
            }

            return View();
        }

        public ActionResult SaveDataSubmit()
        {
            try
            {
                if (this.Request.Form["submit_button"].Equals("OK"))
                {
                    String caseId = Request.Form["caseId"];
                    SessionData sessionData = GetSessionData();
                    InterviewService serviceProxy = ServiceLocator.GetServiceProxy(sessionData.RulebaseName);

                    String[] errors = serviceProxy.SaveCase(sessionData.SessionId, caseId);
                    sessionData.CurrentScreen = null;
                    if (errors == null || errors.Length == 0)
                    {
                        sessionData.CaseId = caseId;
                    }
                    else
                    {
                        ViewData["errors"] = errors;
                        return View("Error");
                    }
                }
            }
            catch (Exception ex)
            {
                ViewData["error_message"] = ex.Message;
                return View("Error");
            }

            return Redirect("/Interview/Summary");
        }

    }
}
