﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>
<%@ Import Namespace="ODSClient.Model" %>
<%@ Import Namespace="Oracle.Examples.Web.Application.Utils" %>
<%@ Import Namespace="ODSClient.Model.Data" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Document Display
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Document Generated</h2>
    <%=ViewData["document"] %>
</asp:Content>



