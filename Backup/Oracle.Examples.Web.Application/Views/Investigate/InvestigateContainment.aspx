﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>
<%@ Import Namespace="ODSClient.Model" %>
<%@ Import Namespace="ODSClient.Model.Screen" %>
<%@ Import Namespace="ODSClient.Model.Screen.Controls" %>
<%@ Import Namespace="Oracle.Examples.Web.Application.Utils" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
  Investigate Screen
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <form id="investigate_form"  action="Submit" runat="server">
        <% ClientInterviewScreen screen = (ClientInterviewScreen)ViewData["screen"]; %>
        <h2><%=screen.ScreenTitle %></h2>
        <%if (screen.Errors != null)
          { %>
           <div id="screen_errors" style="color:Red;">
                <p>The following errors were detected:</p>
                <ul>
                <%for (int i = 0; i < screen.Errors.Length; i++)
                  { %>
                  <li><%=screen.Errors[i]%></li>
                <%} %>
                </ul>
           </div>
        <%}%>
        <table width="100%">
        <% List<ScreenControl> controls = screen.ScreenControls;
           controls.ForEach(delegate(ScreenControl control) {
               if (control.Visible)
               {
                   if (control is ContainmentControl)
                   {
                       ContainmentControl containmentControl = (ContainmentControl)control;
                       List<ScreenControl> instanceControls = containmentControl.Controls;
                       int index = 0;
                       instanceControls.ForEach(delegate(ScreenControl eiControl)
                       {
                           if (eiControl is ScreenEntityInstanceControl)
                           {
                               ScreenEntityInstanceControl tempCtrl = (ScreenEntityInstanceControl)eiControl;
                               String instanceId = tempCtrl.ControlId;
                               List<ScreenControl> inputControls = tempCtrl.Controls;
                               int controlsCount = inputControls.Count;

                               if (!instanceId.Equals("--BLANK--"))
                               {
                             %>
                             <tr>
                                <td width="10%"><input type="checkbox" id="remove" name="remove" value="<%=index%>"/>Remove</td>
                                <td width="85%">
                             <%
                                inputControls.ForEach(delegate(ScreenControl temp)
                                {
                                  %>
                                  <%=ScreenUtil.GenerateEntityCollectHtml(temp, instanceId) %>
                                  <%
                                    if (temp.ErrorMessage != null)
                                    { 
                                      %>
                                      <div id="error" style="color:Red;"><%=temp.ErrorMessage%></div>
                                      <%
                                    }
                                    %>
                                    <br />
                                    <%
                                });
                             %>
                              </td>
                            </tr> 
                           <%
                               }
                               index++;
                           }
                       });

                   }
               }
           }); %>
        </table>
        <input type="hidden" id="investigate_flag" name="investigate_flag" value='<%= ViewData["investigate_flag"] %>' />
        <p><input  name="submit_button" id="submit_button" type="submit" value="Add Instance" /><input name="submit_button" id="submit_button" type="submit" value="Remove Instance/s" /></p>
        <p><input name="submit_button" id="submit_button" type="submit" value="Next >" /></p>

    </form>

</asp:Content>
