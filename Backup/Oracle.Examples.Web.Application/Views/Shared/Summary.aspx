﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>
<%@ Import Namespace="ODSClient.Model" %>
<%@ Import Namespace="Oracle.Examples.Web.Application.Utils" %>
<%@ Import Namespace="ODSClient.Model.Screen" %>
<%@ Import Namespace="ODSClient.Model.Screen.Controls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
  Summary Screen
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

 
    <div id="content">
        <% ClientInterviewScreen screen = (ClientInterviewScreen)ViewData["screen"]; %>
        <h2><%=screen.ScreenTitle %></h2>
            <ul>
        <% List<ScreenControl> controls = screen.ScreenControls;
           controls.ForEach(delegate(ScreenControl control) {
               if (control is ClientGoalControl)
               {
                    ClientGoalControl goalControl = (ClientGoalControl)control;
                    Dictionary<String, String> parameters = new Dictionary<String, String>();
                    parameters.Add("goalstate", goalControl.ControlId);
                                      
                    if (!goalControl.Enabled)
                    {
                      %>
                            <li><%=goalControl.Caption%> 
                      <% 
                    }
                    else
                    {
                      %>
                        <li><a href='<%=URLUtil.BuildLink("Investigate", "Investigate", parameters)%>'><%=goalControl.Caption%></a>
                      <%
                    }
                    if (goalControl.Value != null)
                    {
                    %>
                       &nbsp; [<a href='<%=URLUtil.BuildLink("Interview", "DecisionReport", parameters)%>'>Details</a>]
                    <% } %>
                        </li>
               <%
               }
               else if (control is ClientDocumentControl)
               {
                   ClientDocumentControl documentControl = (ClientDocumentControl) control;
                   if (documentControl.Visible)
                   {
                       Dictionary<String, String> parameters = new Dictionary<String, String>();
                       parameters.Add("documentId", documentControl.DocumentId);
                      %>
                        <li><a href='<%=URLUtil.BuildLink("Interview", "GetDocument", parameters)%>'><%=documentControl.Caption%></a>
                      <%
                   }
               }
               else if (control is ClientLabelControl && control.Visible)
               {
                   %>
                   <br/><label><%=control.Caption %></label><br/>
                   <%
               }
           }); %>
            </ul>
    </div>
</asp:Content>
