﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>
<%@ Import Namespace="ODSClient.Model" %>
<%@ Import Namespace="Oracle.Examples.Web.Application.Utils" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Home Page
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Welcome! Click on the desired rulebase locale to start the interview session.</h2>
    <% if (ViewData["rulebases"] == null)
       { %>
    <div id="no_rulebase">
        <p>
            No rulebase to display.
        </p>
    </div>
    <% }
       else
       { %>
    <div id="rulebase_table">
        
        <table style="width:75%;">
            <tr>
                <td style="width:75%;">
                    Rulebase Name</td>
                <td style="width:25%;">
                    Available Locales</td>
            </tr>
            <%foreach (var temp in (List<RulebaseInformation>)ViewData["rulebases"])  {%>
            <tr>
                <td><%=temp.GetName() %></td>
                <td>
                    <ul>
                        <% foreach (var language in temp.GetAvailableLanguages())
                           {
                               Dictionary<String, String> rvDictionary = new Dictionary<String, String>();
                               rvDictionary.Add("rulebase", temp.GetName());
                               rvDictionary.Add("locale", language);                                   
                               String url = URLUtil.BuildLink("Interview", "Start", rvDictionary);
                               %>
                           <li><a href="<%=url%>"> <%=language %></a></li>
                        <% } %>
                    </ul>
                </td>
            </tr>
            <%}%>
        </table>
    </div>
    <%}%>
</asp:Content>
