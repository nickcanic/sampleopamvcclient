﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>
<%@ Import Namespace="ODSClient.Model" %>
<%@ Import Namespace="Oracle.Examples.Web.Application.Utils" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Save Session Data
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Save Session Data</h2>
    <form id="save_case_form"  action="SaveDataSubmit" runat="server">
        <p>Save case as : <input type="text" name="caseId" id="caseId" value='<%=ViewData["currentCaseId"] %>'></p>
        <p>
            <input id="submit_button" name="submit_button" type="submit" value="OK" />
            <input id="submit_button" name="submit_button" type="submit" value="Cancel" />
        </p>

    </form>
</asp:Content>


