﻿using System;
using System.Collections.Generic;
using ODSClient.ODS2.Server;
using ODSClient.Model;

namespace ODSClient.Service.Server
{
    public class ServerService
    {
        public static RuleBase[] GetRulebases()
        {
            odsServer proxy = new odsServer();
            listrulebasesrequest request = new listrulebasesrequest();
            return proxy.ListRulebases(request);
        }

        public static ServerVersionDetails GetODSVersion()
        {
            odsServer proxy = new odsServer();
            getserverinforequest request = new getserverinforequest();
            
            GetServerInfoResponse response = proxy.GetServerInfo(request);

            return new ServerVersionDetails(response.determinationsserverversion,
                response.determinationsengineversion,
                response.interviewengineversion);
        }
    }
}
