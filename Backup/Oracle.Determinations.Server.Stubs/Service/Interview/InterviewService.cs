﻿using System;
using System.Collections.Generic;
using ODSClient.Model;
using ODSClient.Model.Data;
using ODSClient.Model.Screen;
using ODSClient.Model.Screen.Controls;

namespace ODSClient.Service.Interview
{
    public interface InterviewService
    {

        String OpenSession(String locale);
        void CloseSession(String sessionId);
        ClientInterviewScreen GetScreen(String sessionId, String screenId);
        ClientInterviewScreen SetScreen(SessionData sessionData, out bool isSuccessful);
        List<ScreenControl> ListGoals(String sessionId);
        List<ListScreensEntity> ListScreens(String sessionId);
        void Investigate(SessionData sessionData);
        ClientInterviewScreen GetDecisionReport(SessionData sessionData, String goalId);
        GlobalEntity GetUserSetData(String sessionId);
        String GetDocument(String sessionId, String documentId);
        String[] ListCases(String sessionId);
        String[] LoadCase(String sessionId, String caseId);
        String[] SaveCase(String sessionId, String caseId);
        
    }
}
