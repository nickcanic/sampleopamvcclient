﻿using System;
using System.Collections.Generic;
using ODSClient.ODS2.Interview.InterviewServiceTest;
using ODSClient.Model;
using ODSClient.Model.Data;
using ODSClient.Model.Screen;
using ODSClient.Model.Screen.Controls;

namespace ODSClient.Service.Interview.InterviewServiceTest
{
    public class ResponseProcessor
    {
        public static ClientInterviewScreen BuildScreen(InterviewScreen iScreen)
        {
            ClientInterviewScreen clientScreen = null;
            if (iScreen != null)
            {
                clientScreen = new ClientInterviewScreen();
                clientScreen.ScreenId = iScreen.id;
                clientScreen.ScreenTitle = iScreen.title;
                clientScreen.ScreenType = iScreen.type.ToString();
                clientScreen.Errors = iScreen.errorlist;
                if (iScreen.Items != null)
                    clientScreen.ScreenControls = processControls(iScreen.Items);
            }
            return clientScreen;
        }

        public static List<ScreenControl> processControls(ControlBase[] items)
        {
            List<ScreenControl> controls = new List<ScreenControl>(items.Length);
            for (int i = 0; i < items.Length; i++)
            {
                ControlBase item = items[i];
                ScreenControl control = null;
                if (item is GoalControl)
                {
                    GoalControl goalControl = (GoalControl)item;
                    control = new ClientGoalControl();
                    ((ClientGoalControl)control).EntityId = goalControl.entityid;
                    ((ClientGoalControl)control).InstanceId = goalControl.instanceid;
                    ((ClientGoalControl)control).ControlId = goalControl.goalid;
                    ((ClientGoalControl)control).Caption = goalControl.caption;
                    ((ClientGoalControl)control).Enabled = goalControl.isenabled;
                    ((ClientGoalControl)control).UserProvidedCaptionText = goalControl.isuserprovidedcaptiontext;
                    GoalControlGoalvalue goalValue = goalControl.goalvalue;
                    if (!(goalValue.Item is UnknownValue))
                    {
                        ((ClientGoalControl)control).Enabled = false;
                        ((ClientGoalControl)control).Value = goalValue.Item;
                    }
                }
                else if (item is DocumentControl)
                {
                    DocumentControl documentControl = (DocumentControl)item;
                    control = new ClientDocumentControl();
                    ((ClientDocumentControl)control).Caption = documentControl.caption;
                    ((ClientDocumentControl)control).DocumentId = documentControl.documentid;
                    ((ClientDocumentControl)control).IsHtml = documentControl.ishtml;
                    ((ClientDocumentControl)control).Visible = documentControl.isvisible.Equals("true");
                }
                else if (item is InputControlBase)
                {
                    control = ControlFactory.CreateScreenInputControl((InputControlBase)item);
                }
                else if (item is LabelControl)
                {
                    LabelControl labelControl = (LabelControl)item;
                    control = new ClientLabelControl();
                    ((ClientLabelControl)control).Caption = labelControl.caption;
                    ((ClientLabelControl)control).Visible = Boolean.Parse(labelControl.isvisible);

                }
                else if (item is ReferenceRelationshipControl)
                {
                    ReferenceRelationshipControl refControl = (ReferenceRelationshipControl)item;
                    control = new ReferenceControl();

                    control.Caption = refControl.caption;
                    ((ReferenceControl)control).ControlId = refControl.relationshipid;
                    ((ReferenceControl)control).RelationshipId = refControl.relationshipid;
                    ((ReferenceControl)control).SourceEntityId = refControl.sourceentityid;
                    ((ReferenceControl)control).SourceEntityInstanceId = refControl.sourceinstanceid;
                    ((ReferenceControl)control).TargetEntityId = refControl.targetentityid;

                    RelationshipTarget[] targets = refControl.possibletargets;
                    List<ListOption> targetOptions = new List<ListOption>();
                    if (targets != null)
                    {
                        for (int j = 0; j < targets.Length; j++)
                        {
                            RelationshipTarget target = targets[j];
                            targetOptions.Add(new ListOption(target.Value, target.displayvalue, true));
                        }
                        ((ReferenceControl)control).PossibleTarget = targetOptions;
                    }

                }
                else if (item is ContainmentRelationshipControl)
                {
                    ContainmentRelationshipControl containmentControl = (ContainmentRelationshipControl)item;

                    control = new ContainmentControl();
                    control.Caption = containmentControl.caption;
                    ((ContainmentControl)control).RelationshipId = containmentControl.relationshipid;
                    ((ContainmentControl)control).SourceEntityId = containmentControl.sourceentityid;
                    ((ContainmentControl)control).SourceEntityInstanceId = containmentControl.sourceinstanceid;
                    ((ContainmentControl)control).TargetEntityId = containmentControl.targetentityid;

                    EntityInstanceControl[] entityInstanceControls = containmentControl.entityinstancecontrol;
                    if (entityInstanceControls != null && entityInstanceControls.Length > 0)
                    {
                        List<ScreenControl> eiControls = new List<ScreenControl>(entityInstanceControls.Length);
                        for (int j = 0; j < entityInstanceControls.Length; j++)
                        {
                            EntityInstanceControl eiCtrl = entityInstanceControls[j];
                            ScreenEntityInstanceControl seiControl = new ScreenEntityInstanceControl();
                            seiControl.ControlId = eiCtrl.instanceid;
                            seiControl.EntityId = eiCtrl.entityid;
                            seiControl.Visible = Boolean.Parse(eiCtrl.isvisible);
                            seiControl.Controls = processControls(eiCtrl.Items);


                            eiControls.Add(seiControl);
                        }
                        ((ContainmentControl)control).Controls = eiControls;
                    }
                }
                else if (item is DecisionReportControl)
                {
                    DecisionReportControl decisionControl = (DecisionReportControl)item;
                    control = new ClientDecisionReportControl();
                    control.Caption = decisionControl.caption;
                    DecisionReportNodeControl attributeControl = new DecisionReportNodeControl();
                    AttributeDecisionReportNodeControl adrnCtrl = decisionControl.attributenodecontrol;
                    attributeControl.Caption = adrnCtrl.caption;
                    attributeControl.Controls = ProcessAttributeControlItems(adrnCtrl.Items);
                    ((ClientDecisionReportControl)control).AttributeControl = attributeControl;
                }
                if (control != null)
                {
                    controls.Add(control);
                }
            }
            return controls;
        }

        private static List<ScreenControl> ProcessAttributeControlItems(object[] items)
        {
            List<ScreenControl> childNodes = null;
            if (items != null)
            {
                childNodes = new List<ScreenControl>(items.Length);
                for (int i = 0; i < items.Length; i++)
                {
                    DecisionReportNodeControl drCtrl = new DecisionReportNodeControl();
                    object item = items[i];
                    if (item is AttributeDecisionReportNodeControl)
                    {
                        drCtrl.Caption = ((AttributeDecisionReportNodeControl)item).caption;
                        drCtrl.ControlId = ((AttributeDecisionReportNodeControl)item).attributeid;
                        if (((AttributeDecisionReportNodeControl)item).Items != null)
                        {
                            drCtrl.Controls = ProcessAttributeControlItems(((AttributeDecisionReportNodeControl)item).Items);
                        }
                    }
                    else if (item is RelationshipDecisionReportNodeControl)
                    {
                        drCtrl.Caption = ((RelationshipDecisionReportNodeControl)item).caption;
                        drCtrl.ControlId = ((RelationshipDecisionReportNodeControl)item).id;
                        if (((RelationshipDecisionReportNodeControl)item).Items != null)
                        {
                            drCtrl.Controls = ProcessAttributeControlItems(((RelationshipDecisionReportNodeControl)item).Items);
                        }
                    }
                    else if (item is EntityInstanceDecisionReportNodeControl)
                    {
                        drCtrl.Caption = ((EntityInstanceDecisionReportNodeControl)item).caption;
                        drCtrl.ControlId = ((EntityInstanceDecisionReportNodeControl)item).entityid;
                    }

                    childNodes.Add(drCtrl);
                }
            }
            return childNodes;
        }

        public static List<ScreenControl> ExtractGoals(listgoalsresponseEntity[] entities)
        {
            List<ScreenControl> goalControls = new List<ScreenControl>();
            for (int i = 0; i < entities.Length; i++)
            {
                listgoalsresponseEntity entity = entities[i];
                String entityId = entity.id;
                listgoalsresponseEntityInstance[] instances = entity.instance;
                for (int j = 0; j < instances.Length; j++)
                {
                    listgoalsresponseEntityInstance instance = instances[j];
                    String instanceId = instance.id;
                    GoalDefinition[] goals = instance.goal;
                    for (int k = 0; k < goals.Length; k++)
                    {
                        GoalDefinition goal = goals[k];
                        ClientGoalControl goalControl = new ClientGoalControl();
                        goalControl.EntityId = entityId;
                        goalControl.InstanceId = instanceId;
                        goalControl.ControlId = goal.goalid;
                        goalControl.Caption = goal.goaltext;
                        goalControls.Add(goalControl);
                    }
                }
            }
            return goalControls;
        }

        public static List<LocalAttribute> GetAttributes(AttributeType[] attributeTypes)
        {
            List<LocalAttribute> attributes = new List<LocalAttribute>();
            if (attributeTypes != null)
            {
                for (int i = 0; i < attributeTypes.Length; i++)
                {
                    AttributeType attrType = attributeTypes[i];
                    LocalAttribute attribute = new LocalAttribute();
                    attribute.Id = attrType.id;
                    attribute.Type = attrType.type;
                    attribute.Value = attrType.Item;
                    attributes.Add(attribute);
                }
            }
            return attributes;
        }

        public static List<Entity> GetEntities(EntityType[] entityTypes)
        {
            List<Entity> entities = new List<Entity>();
            if (entityTypes != null)
            {
                for (int i = 0; i < entityTypes.Length; i++)
                {
                    EntityType entType = entityTypes[i];
                    Entity entity = new Entity();
                    entity.Id = entType.id;
                    entity.Instances = GetInstances(entType.instance);
                    entities.Add(entity);
                }
            }
            return entities;
        }

        public static List<EntityInstance> GetInstances(EntityInstanceType[] entityInstanceTypes)
        {
            List<EntityInstance> instances = new List<EntityInstance>();
            if (entityInstanceTypes != null)
            {
                for (int i = 0; i < entityInstanceTypes.Length; i++)
                {
                    EntityInstanceType entInstanceType = entityInstanceTypes[i];
                    EntityInstance entity = new EntityInstance();
                    entity.Id = entInstanceType.id;
                    entity.Attributes = GetAttributes(entInstanceType.attribute);
                    entity.Entities = GetEntities(entInstanceType.entity);
                    entity.Relationships = GetRelationships(entInstanceType.relationship);
                    instances.Add(entity);
                }
            }
            return instances;
        }

        public static List<Relationship> GetRelationships(RelationshipType[] relTypes)
        {
            List<Relationship> relationships = new List<Relationship>();
            if (relTypes != null)
            {
                for (int i = 0; i < relTypes.Length; i++)
                {
                    RelationshipType relType = relTypes[i];
                    Relationship relationship = new Relationship();
                    relationship.Id = relType.id;
                    relationship.State = relType.state;
                    if (relType.target != null)
                    {
                        RelationshipTargetType[] targets = relType.target;
                        List<String> targetIds = new List<String>(targets.Length);
                        for (int j = 0; j < targets.Length; j++)
                        {
                            targetIds.Add(targets[j].instanceid);
                        }
                        relationship.Target = targetIds;
                    }
                    relationships.Add(relationship);
                }
            }
            return relationships;
        }
    }
}
