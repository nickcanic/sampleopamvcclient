﻿using System;
using System.Collections.Generic;
using ODSClient.ODS2.Interview.InterviewServiceTest;
using ODSClient.Model.Screen;
using ODSClient.Model.Screen.Controls;
using ODSClient.Util;

namespace ODSClient.Service.Interview.InterviewServiceTest
{
    public class RequestProcessor
    {
        public static InterviewScreen BuildScreen(ClientInterviewScreen screenObj)
        {
            InterviewScreen iScreen = new InterviewScreen();
            iScreen.id = screenObj.ScreenId;
            //process controls
            try
            {
                iScreen.Items = BuildControls(screenObj.ScreenControls);
            }
            catch (Exception)
            {
                screenObj.Errors = new String[] { "An error has been detected" };
            }
            return iScreen;
        }

        private static ControlBase[] BuildControls(List<ScreenControl> controls)
        {
            List<ControlBase> items = new List<ControlBase>(controls.Count);
            for (int i = 0; i < controls.Count; i++)
            {
                ScreenControl temp = controls[i];
                ControlBase item = null;
                try
                {
                    if (temp is InputScreenControl)
                    {
                        InputScreenControl isCtrl = (InputScreenControl)temp;
                        item = ControlFactory.CreateInputControl(isCtrl);

                    }
                    else if (temp is ContainmentControl)
                    {
                        ContainmentControl cCtrl = (ContainmentControl)temp;
                        item = ControlFactory.CreateContainmentControl(cCtrl);
                    }
                    else if (temp is ReferenceControl)
                    {
                        item = ControlFactory.CreateReferenceControl((ReferenceControl)temp);
                    }
                }
                catch (Exception ex)
                {
                    temp.ErrorMessage = ex.Message;
                    throw new Exception();
                }
                if (item != null)
                {
                    items.Add(item);
                }
            }
            return items.ToArray();
        }
    }
}
