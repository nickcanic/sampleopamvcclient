﻿using System;
using System.Collections.Generic;
using ODSClient.ODS2.Interview.InterviewServiceTest;
using ODSClient.Model;
using ODSClient.Model.Data;
using ODSClient.Model.Screen;
using ODSClient.Model.Screen.Controls;
using ODSClient.Util;
using ODSClient.Ext;


namespace ODSClient.Service.Interview.InterviewServiceTest
{
    public class Proxy : InterviewService
    {
        public String OpenSession(String locale)
        {
            opensessionrequest request = new opensessionrequest();
            odsInterviewService_InterviewServiceTest proxy = new odsInterviewService_InterviewServiceTest();
            if (locale != null)
            {
                ODSSoapHeader mainHeader = new ODSSoapHeader();
                mainHeader.locale = locale;
                proxy.customHeader = mainHeader;
            }

            opensessionresponse response = proxy.OpenSession(request);
            String sessionId = null;
            if (response.success)
            {
                sessionId = response.interviewsessionid;

            }
            return sessionId;
        }

        public void CloseSession(String sessionId)
        {
            BaseStatefulMessage request = new BaseStatefulMessage();
            request.interviewsessionid = sessionId;

            odsInterviewService_InterviewServiceTest proxy = new odsInterviewService_InterviewServiceTest();            
            proxy.CloseSession(request);
        }


        public ClientInterviewScreen GetScreen(String sessionId, String screenId)
        {
            getscreenrequest request = new getscreenrequest();
            request.interviewsessionid = sessionId;
            request.screenid = screenId;

            InterviewScreen responseScreen = null;
            try
            {
                odsInterviewService_InterviewServiceTest proxy = new odsInterviewService_InterviewServiceTest();
                getscreenresponse response = proxy.GetScreen(request);
                responseScreen = response.screen;
            }
            catch (Exception)
            {
                //possible reason for exception: screen not found
            }

            ClientInterviewScreen newScreen = null;
            if (responseScreen != null)
            {
                newScreen = ResponseProcessor.BuildScreen(responseScreen);
            }
            return newScreen;
        }

        public ClientInterviewScreen SetScreen(SessionData sessionData, out bool isSuccessful)
        {
            sessionData.CurrentScreen.Errors = null;

            setscreenrequest request = new setscreenrequest();
            request.interviewsessionid = sessionData.SessionId;
            request.screen = RequestProcessor.BuildScreen(sessionData.CurrentScreen);
            if (sessionData.CurrentScreen.Errors != null)
            {
                isSuccessful = false;
                return sessionData.CurrentScreen;
            }

            odsInterviewService_InterviewServiceTest proxy = new odsInterviewService_InterviewServiceTest();
            setscreenresponse response = proxy.SetScreen(request);

            InterviewScreen responseScreen = response.screen;
            isSuccessful = response.success;

            ClientInterviewScreen newScreen = null;
            if (responseScreen != null)
            {
                newScreen = ResponseProcessor.BuildScreen(responseScreen);
            }
            return newScreen;
        }

        public List<ScreenControl> ListGoals(String sessionId)
        {
            BaseStatefulMessage request = new BaseStatefulMessage();
            request.interviewsessionid = sessionId;

            odsInterviewService_InterviewServiceTest proxy = new odsInterviewService_InterviewServiceTest();
            listgoalsresponse response = proxy.ListGoals(request);

            return ResponseProcessor.ExtractGoals(response.entity);
        }

        //at the moment, can only process html output
        public String GetDocument(String sessionId, String documentId)
        {
            getdocumentrequest request = new getdocumentrequest();
            request.interviewsessionid = sessionId;
            request.documentid = documentId;

            odsInterviewService_InterviewServiceTest proxy = new odsInterviewService_InterviewServiceTest();
            getdocumentresponse response = proxy.GetDocument(request);

            String documentString = null;
            if (response.mimetype.Equals("text/html"))
            {
                byte[] contents = response.documentcontent;
                documentString = System.Text.Encoding.UTF8.GetString(contents);
            }

            return documentString;
        }

        public List<ListScreensEntity> ListScreens(String sessionId)
        {
            listscreensrequest request = new listscreensrequest();
            request.interviewsessionid = sessionId;

            odsInterviewService_InterviewServiceTest proxy = new odsInterviewService_InterviewServiceTest();
            listscreensresponse response = proxy.ListScreens(request);

            listscreensresponseEntity[] entities = response.entity;
            List<ListScreensEntity> entityList = new List<ListScreensEntity>();
            if (entities != null)
            {
                for(int i=0; i< entities.Length; i++)
                {
                    listscreensresponseEntity entity = entities[i];
                    ListScreensEntity lsEntity = new ListScreensEntity(entity.id);
                    if (entity.instance != null)
                    {
                        List<ListScreensEntityInstance> instances = new List<ListScreensEntityInstance>();
                        for (int j = 0; j < entity.instance.Length; j++)
                        {
                            listscreensresponseEntityInstance entityInstance = entity.instance[j];
                            ListScreensEntityInstance lsEntityInstance = new ListScreensEntityInstance(entityInstance.id);
                            if (entityInstance.screen != null)
                            {
                                List<ClientInterviewScreen> screens = new List<ClientInterviewScreen>();
                                for (int k = 0; k < entityInstance.screen.Length; k++)
                                {
                                    InterviewScreen screen = entityInstance.screen[k];
                                    //ignore Data Review screens for now
                                    if (!screen.id.StartsWith("dr"))
                                    {
                                        ClientInterviewScreen clientScreen = new ClientInterviewScreen();
                                        clientScreen.ScreenId = screen.id;
                                        clientScreen.ScreenTitle = screen.title;
                                        screens.Add(clientScreen);
                                    }
                                }
                                lsEntityInstance.Screens = screens;
                            }
                            instances.Add(lsEntityInstance);
                        }
                        lsEntity.Instances = instances;
                    }
                    entityList.Add(lsEntity);
                }                
            }

            return entityList;
        }

        public void Investigate(SessionData sessionData)
        {
            investigaterequest request = new investigaterequest();
            request.interviewsessionid = sessionData.SessionId;
            request.goalstate = sessionData.CurrentGoalState;
            request.includecommentary = false;
            request.includecommentarySpecified = false;

            ClientInterviewScreen screen = sessionData.CurrentScreen;
            if (screen != null)
            {
                screen.Errors = null;
                request.screen = RequestProcessor.BuildScreen(screen);
                if (screen.Errors != null)
                {
                    return;
                }
            }

            odsInterviewService_InterviewServiceTest proxy = new odsInterviewService_InterviewServiceTest();
            investigateresponse response = proxy.Investigate(request);

            ClientInterviewScreen newScreen = ResponseProcessor.BuildScreen(response.screen);
            sessionData.CurrentScreen = newScreen;
            sessionData.CurrentGoalState = response.goalstate;
        }


        public ClientInterviewScreen GetDecisionReport(SessionData sessionData, String goalId)
        {
            getdecisionreportrequest request = new getdecisionreportrequest();
            request.interviewsessionid = sessionData.SessionId;
            request.goalid = goalId;
            request.showinvisible = true;
            request.showsilent = true;

            odsInterviewService_InterviewServiceTest proxy = new odsInterviewService_InterviewServiceTest();
            getdecisionreportresponse response = proxy.GetDecisionReport(request);

            return ResponseProcessor.BuildScreen(response.screen);;
        }

        public GlobalEntity GetUserSetData(String sessionId)
        {
            BaseStatefulMessage request = new BaseStatefulMessage();
            request.interviewsessionid = sessionId;

            odsInterviewService_InterviewServiceTest proxy = new odsInterviewService_InterviewServiceTest();
            getusersetdataresponse response = proxy.GetUserSetData(request);

            GlobalInstanceType globalInstance = response.globalinstance;
            GlobalEntity global = new GlobalEntity();
            global.Id = "global";
            global.Attributes = ResponseProcessor.GetAttributes(globalInstance.attribute);
            global.Entities = ResponseProcessor.GetEntities(globalInstance.entity);
            global.Relationships = ResponseProcessor.GetRelationships(globalInstance.relationship);

            return global;
        }

        public String[] ListCases(String sessionId)
        {
            BaseStatefulMessage request = new BaseStatefulMessage();
            request.interviewsessionid = sessionId;

            odsInterviewService_InterviewServiceTest proxy = new odsInterviewService_InterviewServiceTest();
            listcasesresponse response = proxy.ListCases(request);
            return response.caseid;
        }

        public String[] LoadCase(String sessionId, String caseId)
        {
            loadcaserequest request = new loadcaserequest();
            request.interviewsessionid = sessionId;
            request.caseid = caseId;

            odsInterviewService_InterviewServiceTest proxy = new odsInterviewService_InterviewServiceTest();
            loadcaseresponse response = proxy.LoadCase(request);

            return response.errorlist;
        }

        public String[] SaveCase(String sessionId, String caseId)
        {
            savecaserequest request = new savecaserequest();
            request.interviewsessionid = sessionId;
            request.caseid = caseId;

            odsInterviewService_InterviewServiceTest proxy = new odsInterviewService_InterviewServiceTest();
            savecaseresponse response = proxy.SaveCase(request);
            return response.errorlist;
        }

        #region private members

        private List<ListGoalsEntity> GetGoalsEntityList(listgoalsresponseEntity[] entities)
        {
            List<ListGoalsEntity> entitiesList = new List<ListGoalsEntity>(entities.Length);

            for (int i = 0; i < entities.Length; i++)
            {
                List<ListGoalsEntityInstance> instancesList = GetGoalsEntityInstanceList(entities[i].instance);

                entitiesList.Add(new ListGoalsEntity(entities[i].id, instancesList));
            }

            return entitiesList;
        }

        private List<ListGoalsEntityInstance> GetGoalsEntityInstanceList(listgoalsresponseEntityInstance[] instances)
        {
            List<ListGoalsEntityInstance> instancesList = new List<ListGoalsEntityInstance>(instances.Length);

            for (int i = 0; i < instances.Length; i++)
            {
                instancesList.Add(ProcessGoalEntityInstance(instances[i]));
            }

            return instancesList;
        }

        private ListGoalsEntityInstance ProcessGoalEntityInstance(listgoalsresponseEntityInstance lgsInstance)
        {

            int goalsCount = lgsInstance.goal.Length;
            List<Goal> goals = new List<Goal>(goalsCount);

            for (int i = 0; i < goalsCount; i++)
            {
                goals.Add(new Goal(lgsInstance.goal[i].attributeid, lgsInstance.goal[i].goalid,
                    lgsInstance.goal[i].goaltext, lgsInstance.goal[i].hasdecisionreport));
            }
            return new ListGoalsEntityInstance(lgsInstance.id, goals);
        }
        #endregion
    }
}
