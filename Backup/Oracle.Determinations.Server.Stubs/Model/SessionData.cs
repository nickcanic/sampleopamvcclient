﻿using System;
using System.Collections.Generic;
using ODSClient.Model.Screen;

namespace ODSClient.Model
{
    //Class that contains session information
    public class SessionData
    {
        private String sessionId = null;
        private String rulebaseName = null;
        private String sessionLocale = null;
        private String currentGoalState = null;
        private ClientInterviewScreen currentScreen = null;
        private String caseId = null;

        public SessionData(String sessionId)
        {
            SessionId = sessionId;
        }

        public String SessionId {
            get
            {
                return this.sessionId;
            }

            set
            {
                this.sessionId = value;
            }
        }

        public String RulebaseName
        {
            get
            {
                return this.rulebaseName;
            }

            set
            {
                this.rulebaseName = value;
            }
        }

        public String Locale
        {
            get
            {
                return this.sessionLocale;
            }

            set
            {
                this.sessionLocale = value;
            }
        }

        public String CurrentGoalState
        {
            get
            {
                return this.currentGoalState;
            }

            set
            {
                this.currentGoalState = value;
            }
        }

        public ClientInterviewScreen CurrentScreen
        {
            get
            {
                return this.currentScreen;
            }

            set
            {
                this.currentScreen = value;
            }
        }

        public String CaseId
        {
            get
            {
                return this.caseId;
            }

            set
            {
                this.caseId = value;
            }
        }
    }
}
