﻿using System;
using System.Collections.Generic;
using ODSClient.Service.Server;
using ODSClient.ODS2.Server;


namespace ODSClient.Model
{
    public class ServerInformation
    {
        public List<RulebaseInformation> GetRulebases()
        {
            RuleBase[] rulebases = ServerService.GetRulebases();
            List<RulebaseInformation> rulebaseInfoList = null;
            if (rulebases != null)
            {
                rulebaseInfoList = new List<RulebaseInformation>(rulebases.Length);
                for (int i=0; i< rulebases.Length; i++)
                {
                    RuleBase temp = rulebases[i];
                    RulebaseInformation rbInfo = new RulebaseInformation(temp.name);
                    LanguageType[] langTypes = temp.availablelanguages;
                    for (int j = 0; j < langTypes.Length; j++)
                    {
                        rbInfo.AddLanguage(langTypes[j].Value);
                    }
                    rulebaseInfoList.Add(rbInfo);

                }
            }
            return rulebaseInfoList;
        }

        public ServerVersionDetails GetVersionDetails()
        {
            return ServerService.GetODSVersion();
        }
    }

    public class RulebaseInformation
    {
        String rulebaseName;
        List<String> availableLanguages;

        public RulebaseInformation(String name)
        {
            rulebaseName = name;
            availableLanguages = new List<string>();
        }

        public void AddLanguage(String language)
        {
            availableLanguages.Add(language);
        }

        public String GetName()
        {
            return rulebaseName;
        }

        public List<String> GetAvailableLanguages()
        {
            return availableLanguages;
        }
    }

    public class ServerVersionDetails
    {
        private String odsVersion;
        private String engineVersion;
        private String intervewEngineVersion;

        public ServerVersionDetails(String odsVersion, String engineVersion, String intervewEngineVersion)
        {
            this.odsVersion = odsVersion;
            this.engineVersion = engineVersion;
            this.intervewEngineVersion = intervewEngineVersion;
        }

        public String GetDeterminationsServerVersion()
        {
            return this.odsVersion;
        }

        public String GetDeterminationsEngineVersion()
        {
            return this.engineVersion;
        }

        public String GetInterviewEngineVersion()
        {
            return this.intervewEngineVersion;
        }
    }
}
