﻿using System;
using System.Collections.Generic;
using System.Text;
using ODSClient.Model.Screen;

namespace ODSClient.Model
{

    public class ListScreensEntity
    {
        private String id = null;
        private List<ListScreensEntityInstance> instances = null;

        public ListScreensEntity(String id)
        {
            this.id = id;
        }

        public String GetId()
        {
            return this.id;
        }

        public List<ListScreensEntityInstance> Instances
        {
            get
            {
                return this.instances;
            }

            set
            {
                this.instances = value;
            }
        }
    }

    public class ListScreensEntityInstance
    {
        private String id = null;
        private List<ClientInterviewScreen> screens = null;

        public ListScreensEntityInstance(String id)
        {
            this.id = id;
        }

        public String GetId()
        {
            return this.id;
        }

        public List<ClientInterviewScreen> Screens
        {
            get
            {
                return this.screens;
            }

            set
            {
                this.screens = value;
            }
        }
    }

}
