﻿using System;
using System.Collections.Generic;
using System.Web.Services;
using System.Web.Services.Protocols;

namespace ODSClient.Ext
{
    [System.Xml.Serialization.XmlRoot("international", Namespace = "http://www.w3.org/2005/09/ws-i18n")]
    public class ODSSoapHeader : SoapHeader
    {
        public String locale;
        public String tz;
    }
}
