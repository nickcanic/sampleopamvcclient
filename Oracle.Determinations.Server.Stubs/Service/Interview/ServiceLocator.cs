﻿using System;
using System.Collections.Generic;
using ODSClient.Model.Error;
using ConfigurationManager = System.Configuration.ConfigurationManager;

namespace ODSClient.Service.Interview
{
    public class ServiceLocator
    {
        public static InterviewService GetServiceProxy(String rulebase)
        {
            String assemblyName = ConfigurationManager.AppSettings["service.client.assembly." + rulebase];
            String proxyName = ConfigurationManager.AppSettings["service.client.proxy." + rulebase];
            if (assemblyName == null || proxyName == null)
            {
                throw new MissingConfigurationException("Assembly name or type name for "+ rulebase +
                    " proxy class not found in configuration file.");
            }

            InterviewService proxy = null;
            try
            {
                proxy = (InterviewService)System.Activator.CreateInstance(assemblyName, proxyName).Unwrap();
            }
            catch (Exception e)
            {
                throw new ServiceNotFoundException(rulebase, "Error in creating instance of : " + proxyName);
            }

            if (proxy == null)
            {
                throw new ServiceNotFoundException(rulebase);
            }
            return proxy;
        }

    }
}
