﻿using System;
using System.Collections.Generic;
using ODSClient.ODS2.Interview.InterviewServiceTest;
using ODSClient.Model.Screen.Controls;
using ODSClient.Model.Data;

namespace ODSClient.Service.Interview.InterviewServiceTest
{
    public class ControlFactory
    {
        public static ContainmentRelationshipControl CreateContainmentControl(ContainmentControl ctrl)
        {
            ContainmentRelationshipControl crCtrl = new ContainmentRelationshipControl();
            crCtrl.relationshipid = ctrl.RelationshipId;
            crCtrl.sourceentityid = ctrl.SourceEntityId;
            crCtrl.targetentityid = ctrl.TargetEntityId;
            crCtrl.sourceinstanceid = ctrl.SourceEntityInstanceId;
            List<ScreenControl> childControls = ctrl.Controls;
            if (childControls != null)
            {
                List<EntityInstanceControl> eiCtrls = new List<EntityInstanceControl>(childControls.Count);

                for (int i = 0; i < childControls.Count; i++)
                {
                    if (childControls[i] is ScreenEntityInstanceControl)
                    {
                        ScreenEntityInstanceControl seiCtrl = (ScreenEntityInstanceControl)childControls[i];
                        if (!seiCtrl.ControlId.Equals("--BLANK--"))
                        {
                            EntityInstanceControl eiCtrl = new EntityInstanceControl();
                            eiCtrl.entityid = seiCtrl.EntityId;
                            eiCtrl.instanceid = seiCtrl.ControlId;

                            List<ScreenControl> sInputCtrls = seiCtrl.Controls;
                            if (sInputCtrls != null)
                            {
                                List<ControlBase> inputCtrls = new List<ControlBase>(sInputCtrls.Count);
                                sInputCtrls.ForEach(delegate(ScreenControl temp)
                                {
                                    try
                                    {
                                        if (temp is InputScreenControl)
                                            inputCtrls.Add(CreateInputControl((InputScreenControl)temp));
                                        else if (temp is ReferenceControl)
                                            inputCtrls.Add(CreateReferenceControl((ReferenceControl)temp));
                                    }
                                    catch (Exception ex)
                                    {
                                        temp.ErrorMessage = ex.Message;
                                        throw new Exception();
                                    }
                                });
                                eiCtrl.Items = inputCtrls.ToArray();
                            }
                            eiCtrls.Add(eiCtrl);
                        }
                    }
                }
                crCtrl.entityinstancecontrol = eiCtrls.ToArray();
            }
            return crCtrl;
        }

        public static ReferenceRelationshipControl CreateReferenceControl(ReferenceControl referenceControl)
        {
            ReferenceRelationshipControl refCtrl = null;
            if (referenceControl != null)
            {
                refCtrl = new ReferenceRelationshipControl();
                refCtrl.relationshipid = referenceControl.RelationshipId;
                refCtrl.sourceentityid = referenceControl.SourceEntityId;
                refCtrl.targetentityid = referenceControl.TargetEntityId;
                refCtrl.sourceinstanceid = referenceControl.SourceEntityInstanceId;
                String targetInstanceId = referenceControl.TargetEntityInstanceId;

                if (targetInstanceId != null && targetInstanceId.Length > 0)
                {
                    RelationshipTarget target = new RelationshipTarget();
                    target.Value = targetInstanceId;
                    refCtrl.settargets = new RelationshipTarget[] { target};
                }

            }
            return refCtrl;
        }

        public static InputControlBase CreateInputControl(InputScreenControl screenControl)
        {
            InputControlBase control = null;
            InputControlType controlType = screenControl.GetControlType();
            switch (controlType)
            {
                case InputControlType.Boolean:
                    control = new BooleanControl();
                    control.attributeid = screenControl.ControlId;
                    ((BooleanControl)control).currentvalue = (BooleanControlValue)GetValue(controlType, screenControl.Value);

                    break;
                case InputControlType.Text:
                    control = new TextControl();
                    control.attributeid = screenControl.ControlId;
                    ((TextControl)control).currentvalue = (TextControlValue)GetValue(controlType, screenControl.Value);
                    break;
                case InputControlType.Number:
                    control = new NumberControl();
                    control.attributeid = screenControl.ControlId;
                    ((NumberControl)control).currentvalue = (NumberControlValue)GetValue(controlType, screenControl.Value);
                    break;
                case InputControlType.Currency:
                    control = new CurrencyControl();
                    control.attributeid = screenControl.ControlId;
                    ((CurrencyControl)control).currentvalue = (NumberControlValue)GetValue(controlType, screenControl.Value);
                    break;
                case InputControlType.Date:
                    control = new DateControl();
                    control.attributeid = screenControl.ControlId;
                    ((DateControl)control).currentvalue = (DateControlValue)GetValue(controlType, screenControl.Value);
                    break;
                case InputControlType.DateTime:
                    control = new DateTimeControl();
                    control.attributeid = screenControl.ControlId;
                    ((DateTimeControl)control).currentvalue = (DateTimeControlValue)GetValue(controlType, screenControl.Value);
                    break;
                case InputControlType.TimeOfDay:
                    control = new TimeOfDayControl();
                    control.attributeid = screenControl.ControlId;
                    ((TimeOfDayControl)control).currentvalue = (TimeControlValue)GetValue(controlType, screenControl.Value);
                    break;
                default: break;
            }
            return control;
        }

        private static Object GetValue(InputControlType type, Object value)
        {
            Object inputValue = null;
            switch (type)
            {
                case InputControlType.Boolean:
                    inputValue = new BooleanControlValue();
                    if (value is Uncertain)
                    {
                        ((BooleanControlValue)inputValue).Item = new UncertainValue();
                    } else if(value is Unknown)
                    {
                        ((BooleanControlValue)inputValue).Item = new UnknownValue();
                    }
                    else
                    {
                        ((BooleanControlValue)inputValue).Item = Boolean.Parse(value.ToString());
                    }
                    break;
                case InputControlType.Text:
                    inputValue = new TextControlValue();
                    if (value is Uncertain)
                    {
                        ((TextControlValue)inputValue).Item = new UncertainValue();
                    }
                    else if (value is Unknown)
                    {
                        ((TextControlValue)inputValue).Item = new UnknownValue();
                    }
                    else
                    {
                        ((TextControlValue)inputValue).Item = value.ToString();
                    }
                    break;
                case InputControlType.Number:

                case InputControlType.Currency:
                    inputValue = new NumberControlValue();
                    if (value is Uncertain)
                    {
                        ((NumberControlValue)inputValue).Item = new UncertainValue();
                    }
                    else if (value is Unknown)
                    {
                        ((NumberControlValue)inputValue).Item = new UnknownValue();
                    }
                    else
                    {
                        ((NumberControlValue)inputValue).Item = Decimal.Parse(value.ToString());
                    }
                    break;
                case InputControlType.Date:
                    inputValue = new DateControlValue();
                    if (value is Uncertain)
                    {
                        ((DateControlValue)inputValue).Item = new UncertainValue();
                    }
                    else if (value is Unknown)
                    {
                        ((DateControlValue)inputValue).Item = new UnknownValue();
                    }
                    else
                    {
                        ((DateControlValue)inputValue).Item = DateTime.Parse(value.ToString());
                    }
                    break;
                case InputControlType.DateTime:
                    inputValue = new DateTimeControlValue();
                    if (value is Uncertain)
                    {
                        ((DateTimeControlValue)inputValue).Item = new UncertainValue();
                    }
                    else if (value is Unknown)
                    {
                        ((DateTimeControlValue)inputValue).Item = new UnknownValue();
                    }
                    else
                    {
                        ((DateTimeControlValue)inputValue).Item = DateTime.Parse(value.ToString());
                    }
                    break;
                case InputControlType.TimeOfDay:
                    inputValue = new TimeControlValue();
                    if (value is Uncertain)
                    {
                        ((TimeControlValue)inputValue).Item = new UncertainValue();
                    }
                    else if (value is Unknown)
                    {
                        ((TimeControlValue)inputValue).Item = new UnknownValue();
                    }
                    else
                    {
                        ((TimeControlValue)inputValue).Item = DateTime.Parse(value.ToString());
                    }
                    break;
                default: break;
            }
            return inputValue;
        }

        #region InputControlBase to InputScreenControl conversion
        public static InputScreenControl CreateScreenInputControl(InputControlBase control)
        {
            InputScreenControl screenControl = null;
            if (control is TextControl)
            {
                TextControl textControl = (TextControl)control;
                screenControl = new InputScreenControl(InputControlType.Text);
                screenControl.ControlId = textControl.attributeid;
                screenControl.Caption = textControl.caption;
                screenControl.Visible = Boolean.Parse(textControl.isvisible);
                screenControl.Mandatory = Boolean.Parse(textControl.ismandatory);


                screenControl.Value = GetInputControlValue(textControl, textControl.currentvalue.Item);
                screenControl.DefaultValue = GetInputControlValue(textControl, textControl.defaultvalue.Item);

                TextOption[] options = textControl.listoptions;
                if (options != null)
                {
                    List<ListOption> optionsList = new List<ListOption>(options.Length);
                    for (int i = 0; i < options.Length; i++)
                    {
                        TextOption option = options[i];
                        if (option.isvisible)
                        {
                            String displayText = option.displaytext;
                            Object value = option.Item;
                            Object listOptionValue = null;
                            if (value is UnknownValue)
                            {
                                listOptionValue = new Unknown();
                            }
                            else if (value is UncertainValue)
                            {
                                listOptionValue = new Uncertain();
                            }
                            else
                            {
                                listOptionValue = value;
                            }
                            optionsList.Add(new ListOption(listOptionValue, option.displaytext, option.isvisible));
                        }
                    }
                    screenControl.Options = optionsList;
                }

                screenControl.ErrorMessage = GetErrors(textControl.errorlist);
            }
            else if (control is BooleanControl)
            {
                BooleanControl booleanControl = (BooleanControl)control;
                screenControl = new InputScreenControl(InputControlType.Boolean);
                screenControl.ControlId = booleanControl.attributeid;
                screenControl.Caption = booleanControl.caption;
                screenControl.Visible = Boolean.Parse(booleanControl.isvisible);
                screenControl.Mandatory = Boolean.Parse(booleanControl.ismandatory);

                screenControl.Value = GetInputControlValue(booleanControl, booleanControl.currentvalue.Item);
                screenControl.DefaultValue = GetInputControlValue(booleanControl, booleanControl.defaultvalue.Item);


                BooleanOption[] options = booleanControl.listoptions;
                if (options != null)
                {
                    List<ListOption> optionsList = new List<ListOption>(options.Length);
                    for (int i = 0; i < options.Length; i++)
                    {
                        BooleanOption option = options[i];
                        if (option.isvisible)
                        {
                            String displayText = option.displaytext;
                            Object value = option.Item;
                            Object listOptionValue = null;
                            if (value is UnknownValue)
                            {
                                listOptionValue = new Unknown();
                            }
                            else if (value is UncertainValue)
                            {
                                listOptionValue = new Uncertain();
                            }
                            else
                            {
                                listOptionValue = value;
                            }
                            optionsList.Add(new ListOption(listOptionValue, option.displaytext, option.isvisible));
                        }
                    }
                    screenControl.Options = optionsList;
                }
                screenControl.ErrorMessage = GetErrors(booleanControl.errorlist);

            }
            else if (control is NumberControl)
            {
                NumberControl numberControl = (NumberControl)control;
                screenControl = new InputScreenControl(InputControlType.Number);
                screenControl.ControlId = numberControl.attributeid;
                screenControl.Caption = numberControl.caption;
                screenControl.Visible = Boolean.Parse(numberControl.isvisible);
                screenControl.Mandatory = Boolean.Parse(numberControl.ismandatory);

                screenControl.Value = GetInputControlValue(numberControl, numberControl.currentvalue.Item);
                screenControl.DefaultValue = GetInputControlValue(numberControl, numberControl.defaultvalue.Item);

                screenControl.Options = GetOptionsForNumberControl(numberControl.listoptions);
                screenControl.ErrorMessage = GetErrors(numberControl.errorlist);
            }
            else if (control is CurrencyControl)
            {
                CurrencyControl currencyControl = (CurrencyControl)control;
                screenControl = new InputScreenControl(InputControlType.Currency);
                screenControl.ControlId = currencyControl.attributeid;
                screenControl.Caption = currencyControl.caption;
                screenControl.Visible = Boolean.Parse(currencyControl.isvisible);
                screenControl.Mandatory = Boolean.Parse(currencyControl.ismandatory);

                screenControl.Value = GetInputControlValue(currencyControl, currencyControl.currentvalue.Item);
                screenControl.DefaultValue = GetInputControlValue(currencyControl, currencyControl.defaultvalue.Item);

                screenControl.Options = GetOptionsForNumberControl(currencyControl.listoptions);
                screenControl.ErrorMessage = GetErrors(currencyControl.errorlist);
            }
            else if (control is DateControl)
            {
                DateControl dateControl = (DateControl)control;
                screenControl = new InputScreenControl(InputControlType.Date);
                screenControl.ControlId = dateControl.attributeid;
                screenControl.Caption = dateControl.caption;
                screenControl.Visible = Boolean.Parse(dateControl.isvisible);
                screenControl.Mandatory = Boolean.Parse(dateControl.ismandatory);

                screenControl.Value = GetInputControlValue(dateControl, dateControl.currentvalue.Item);
                screenControl.DefaultValue = GetInputControlValue(dateControl, dateControl.defaultvalue.Item);

                DateOption[] options = dateControl.listoptions;
                if (options != null)
                {
                    List<ListOption> optionsList = new List<ListOption>(options.Length);
                    for (int i = 0; i < options.Length; i++)
                    {
                        DateOption option = options[i];
                        if (option.isvisible)
                        {
                            String displayText = option.displaytext;
                            Object value = option.Item;
                            Object listOptionValue = null;
                            if (value is UnknownValue)
                            {
                                listOptionValue = new Unknown();
                            }
                            else if (value is UncertainValue)
                            {
                                listOptionValue = new Uncertain();
                            }
                            else
                            {
                                listOptionValue = value;
                            }
                            optionsList.Add(new ListOption(listOptionValue, option.displaytext, option.isvisible));
                        }
                    }
                    screenControl.Options = optionsList;
                }

                screenControl.ErrorMessage = GetErrors(dateControl.errorlist);
            }
            else if (control is DateTimeControl)
            {
                DateTimeControl dateControl = (DateTimeControl)control;
                screenControl = new InputScreenControl(InputControlType.DateTime);
                screenControl.ControlId = dateControl.attributeid;
                screenControl.Caption = dateControl.caption;
                screenControl.Visible = Boolean.Parse(dateControl.isvisible);
                screenControl.Mandatory = Boolean.Parse(dateControl.ismandatory);

                screenControl.Value = GetInputControlValue(dateControl, dateControl.currentvalue.Item);
                screenControl.DefaultValue = GetInputControlValue(dateControl, dateControl.defaultvalue.Item);

                DateTimeOption[] options = dateControl.listoptions;
                if (options != null)
                {
                    List<ListOption> optionsList = new List<ListOption>(options.Length);
                    for (int i = 0; i < options.Length; i++)
                    {
                        DateTimeOption option = options[i];
                        if (option.isvisible)
                        {
                            String displayText = option.displaytext;
                            Object value = option.Item;
                            Object listOptionValue = null;
                            if (value is UnknownValue)
                            {
                                listOptionValue = new Unknown();
                            }
                            else if (value is UncertainValue)
                            {
                                listOptionValue = new Uncertain();
                            }
                            else
                            {
                                listOptionValue = value;
                            }
                            optionsList.Add(new ListOption(listOptionValue, option.displaytext, option.isvisible));
                        }
                    }
                    screenControl.Options = optionsList;
                }

                screenControl.ErrorMessage = GetErrors(dateControl.errorlist);
            }
            else if (control is TimeOfDayControl)
            {
                TimeOfDayControl dateControl = (TimeOfDayControl)control;
                screenControl = new InputScreenControl(InputControlType.DateTime);
                screenControl.ControlId = dateControl.attributeid;
                screenControl.Caption = dateControl.caption;
                screenControl.Visible = Boolean.Parse(dateControl.isvisible);
                screenControl.Mandatory = Boolean.Parse(dateControl.ismandatory);

                screenControl.Value = GetInputControlValue(dateControl, dateControl.currentvalue.Item);
                screenControl.DefaultValue = GetInputControlValue(dateControl, dateControl.defaultvalue.Item);

                TimeOption[] options = dateControl.listoptions;
                if (options != null)
                {
                    List<ListOption> optionsList = new List<ListOption>(options.Length);
                    for (int i = 0; i < options.Length; i++)
                    {
                        TimeOption option = options[i];
                        if (option.isvisible)
                        {
                            String displayText = option.displaytext;
                            Object value = option.Item;
                            Object listOptionValue = null;
                            if (value is UnknownValue)
                            {
                                listOptionValue = new Unknown();
                            }
                            else if (value is UncertainValue)
                            {
                                listOptionValue = new Uncertain();
                            }
                            else
                            {
                                listOptionValue = value;
                            }
                            optionsList.Add(new ListOption(listOptionValue, option.displaytext, option.isvisible));
                        }
                    }
                    screenControl.Options = optionsList;
                }

                screenControl.ErrorMessage = GetErrors(dateControl.errorlist);
            }

            return screenControl;
        }

        private static List<ListOption> GetOptionsForNumberControl(NumberOption[] numberOptions)
        {
            if (numberOptions != null)
            {
                List<ListOption> optionsList = new List<ListOption>(numberOptions.Length);
                for (int i = 0; i < numberOptions.Length; i++)
                {
                    NumberOption option = numberOptions[i];
                    if (option.isvisible)
                    {
                        String displayText = option.displaytext;
                        Object value = option.Item;
                        Object listOptionValue = null;
                        if (value is UnknownValue)
                        {
                            listOptionValue = new Unknown();
                        }
                        else if (value is UncertainValue)
                        {
                            listOptionValue = new Uncertain();
                        }
                        else
                        {
                            listOptionValue = value;
                        }
                        optionsList.Add(new ListOption(listOptionValue, option.displaytext, option.isvisible));
                    }
                }

                return optionsList;
            }

            return null;
        }

        private static Object GetInputControlValue(InputControlBase inputControl, Object controlValue)
        {
            Object value = null;
            //unknown or uncertain else parse
            if (controlValue is UnknownValue)
            {
                value = new Unknown();

            }
            else if (controlValue is UncertainValue)
            {
                value = new Uncertain();
            }
            else
            {
                if (inputControl is DateControl)
                {
                    value = ((DateTime)controlValue).ToString("MM/dd/yyyy");
                }
                else if (inputControl is DateTimeControl)
                {
                    value = ((DateTime)controlValue).ToString("MM/dd/yyyy hh:mm:ss");
                }
                else if (inputControl is TimeOfDayControl)
                {
                    value = ((DateTime)controlValue).ToString("hh:mm:ss");
                }
                else
                {
                    value = controlValue.ToString();
                }
            }
            return value;
        }
        #endregion

        private static String GetErrors(String[] errorMsgs)
        {
            String errorMessage = null;
            if (errorMsgs != null)
            {
                for (int i = 0; i < errorMsgs.Length; i++)
                {
                    errorMessage += errorMsgs[i];
                }
            }
            return errorMessage;
        }
    }
}
