﻿using System;
using System.Collections.Generic;
using System.Text;
using ODSClient.Util;
using ODSClient.Model.Screen.Controls;

namespace ODSClient.Model.Screen
{
    public class ClientInterviewScreen
    {
        private String screenId = null;
        private String screenTitle = null;
        private String screenType = null;
        private List<ScreenControl> controls = null;
        private String[] errors = null;

        public ClientInterviewScreen()
        {
        }

        public ClientInterviewScreen(String id, String title, String type, bool isAutomatic, List<ScreenControl> controls)
        {
            screenId = id;
            screenTitle = isAutomatic ? ApplicationConstants.AUTOMATIC_SCREEN_TITLE : title;
            screenType = type;
            this.controls = controls;

        }

        public String ScreenId
        {
            get
            {
                return this.screenId;
            }

            set
            {
                this.screenId = value;
            }
        }

        public String ScreenTitle
        {
            get
            {
                return this.screenTitle;
            }

            set
            {
                this.screenTitle = value;
            }
        }

        public String ScreenType
        {
            get
            {
                return this.screenType;
            }

            set
            {
                this.screenType = value;
            }
        }

        public List<ScreenControl> ScreenControls
        {
            get
            {
                return this.controls;
            }

            set
            {
                this.controls = value;
            }
        }

        public String[] Errors
        {
            get
            {
                return this.errors;
            }

            set
            {
                this.errors = value;
            }
        }
    }
}
