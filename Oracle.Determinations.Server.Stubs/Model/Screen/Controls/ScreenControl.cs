﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ODSClient.Model.Screen.Controls
{
    public abstract class ScreenControl
    {
        String id = null;
        String caption = null;
        bool isVisible = true;
        String errorMessage = null;
        public String ControlId
        {
            get
            {
                return this.id;
            }

            set
            {
                this.id = value;
            }
        }


        public String Caption
        {
            get
            {
                return this.caption;
            }

            set
            {
                this.caption = value;
            }
        }

        public bool Visible
        {
            get
            {
                return this.isVisible;
            }

            set
            {
                this.isVisible = value;
            }
        }

        public String ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }

            set
            {
                this.errorMessage = value;
            }
        }
    }

    public class ClientGoalControl : ScreenControl
    {
        private String entityId = null;
        private String instanceId = null;
        private bool isEnabled = true;
        private bool isUserProvidedCaptionText = false;
        private Object value = null;

        public String EntityId
        {
            get
            {
                return this.entityId;
            }

            set
            {
                this.entityId = value;
            }
        }

        public String InstanceId
        {
            get
            {
                return this.instanceId;
            }

            set
            {
                this.instanceId = value;
            }
        }

        public bool Enabled
        {
            get
            {
                return this.isEnabled;
            }

            set
            {
                this.isEnabled = value;
            }
        }

        public bool UserProvidedCaptionText
        {
            get
            {
                return this.isUserProvidedCaptionText;
            }

            set
            {
                this.isUserProvidedCaptionText = value;
            }
        }

        public Object Value
        {
            get
            {
                return this.value;
            }

            set
            {
                this.value = value;
            }
        }
    }

    public class ClientDocumentControl : ScreenControl
    {
        private String documentId = null;
        private bool isHtml = true;

        public String DocumentId
        {
            get
            {
                return this.documentId;
            }
            set
            {
                this.documentId = value;
            }
        }

        public bool IsHtml
        {
            get
            {
                return this.isHtml;
            }
            set
            {
                this.isHtml = value;
            }
        }
    }

    public abstract class GroupControl : ScreenControl
    {
        List<ScreenControl> controls = null;

        public List<ScreenControl> Controls
        {
            get
            {
                return this.controls;
            }

            set
            {
                this.controls = value;
            }
        }

    }

    public class GroupRelationshipControl : GroupControl
    {
        String relationshipId = null;
        String relationshipType = null;
        String sourceEntityId = null;
        String targetEntityId = null;
        String sourceEntityInstanceId = null;
        String targetEntityInstanceId = null;

        public String RelationshipId
        {
            get
            {
                return this.relationshipId;
            }

            set
            {
                this.relationshipId = value;
            }
        }

        public String RelationshipType
        {
            get
            {
                return this.relationshipType;
            }

            set
            {
                this.relationshipType = value;
            }
        }

        public String SourceEntityId
        {
            get
            {
                return this.sourceEntityId;
            }

            set
            {
                this.sourceEntityId = value;
            }
        }

        public String TargetEntityId
        {
            get
            {
                return this.targetEntityId;
            }

            set
            {
                this.targetEntityId = value;
            }
        }

        public String SourceEntityInstanceId
        {
            get
            {
                return this.sourceEntityInstanceId;
            }

            set
            {
                this.sourceEntityInstanceId = value;
            }
        }

        public String TargetEntityInstanceId
        {
            get
            {
                return this.targetEntityInstanceId;
            }

            set
            {
                this.targetEntityInstanceId = value;
            }
        }
    }

    public class ContainmentControl : GroupRelationshipControl
    {

    }

    public class ReferenceControl : GroupRelationshipControl
    {
        public List<ListOption> possibleTarget = new List<ListOption>();

        public List<ListOption> PossibleTarget
        {
            get
            {
                return this.possibleTarget;
            }

            set
            {
                this.possibleTarget = value;
            }
        }
    }

    public class ScreenEntityInstanceControl : GroupControl
    {
        String entityId = null;
        public String EntityId
        {
            get
            {
                return this.entityId;
            }

            set
            {
                this.entityId = value;
            }
        }

    }

    public class InputScreenControl : ScreenControl
    {


        Object value = null;
        Object defaultValue = null;
        bool isMandatory = true;
        InputControlType controlType;
        Dictionary<Object, Object> properties = new Dictionary<Object, Object>();
        List<ListOption> options = null;


        public InputScreenControl(InputControlType type)
        {
            controlType = type;
        }

        public InputControlType GetControlType()
        {
            return controlType;
        }



        public Object Value
        {
            get
            {
                return this.value;
            }

            set
            {
                this.value = value;
            }
        }

        public List<ListOption> Options
        {
            get
            {
                return this.options;
            }

            set
            {
                this.options = value;
            }
        }

        public Object DefaultValue
        {
            get
            {
                return this.defaultValue;
            }

            set
            {
                this.defaultValue = value;
            }
        }

        public bool Mandatory
        {
            get
            {
                return this.isMandatory;
            }

            set
            {
                this.isMandatory = value;
            }
        }



    }

    public class ClientDecisionReportControl : ScreenControl
    {
        private DecisionReportNodeControl attributeControl;

        public DecisionReportNodeControl AttributeControl
        {
            get
            {
                return this.attributeControl;
            }

            set
            {
                this.attributeControl = value;
            }
        }
    }

    public class DecisionReportNodeControl : GroupControl
    {
        private object displayValue = null;

        public object DisplayValue
        {
            get
            {
                return this.displayValue;
            }

            set
            {
                this.displayValue = value;
            }
        }
    }

    public enum InputControlType
    {
        Boolean,
        Text,
        Number,
        Currency,
        Date,
        DateTime,
        TimeOfDay
    }

    public class ListOption
    {
        String displayText = null;
        Object value = null;
        bool isVisible = true;

        public ListOption(Object value, String displayValue, bool isVisible)
        {
            this.value = value;
            this.displayText = displayValue;
            this.isVisible = isVisible;
        }

        public Object GetValue()
        {
            return this.value;
        }

        public String GetDisplayText()
        {
            return this.displayText;
        }

        public bool IsVisible()
        {
            return this.isVisible;
        }
    }

    public class ClientLabelControl : ScreenControl
    {
    }
}
