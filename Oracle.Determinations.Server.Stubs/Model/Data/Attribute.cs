﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ODSClient.Model.Data
{
    public class LocalAttribute
    {
        String attributeId = null;
        String attributeText = null;
        Object attributeValue = null;
        String type=null;

        public String Id
        {
            get
            {
                return this.attributeId;
            }

            set
            {
                this.attributeId = value;
            }
        }

        public String Type
        {
            get
            {
                return this.type;
            }

            set
            {
                this.type = value;
            }
        }

        public String Text
        {
            get
            {
                return this.attributeText;
            }

            set
            {
                this.attributeText = value;
            }
        }

        public Object Value
        {
            get
            {
                return this.attributeValue;
            }

            set
            {
                this.attributeValue = value;
            }
        }
    }
}
