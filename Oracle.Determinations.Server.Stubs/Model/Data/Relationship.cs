﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ODSClient.Model.Data
{
    public class Relationship
    {
        String state = null;
        String id = null;
        List<String> targets = null;

        public String Id
        {
            get
            {
                return this.id;
            }

            set
            {
                this.id = value;
            }
        }

        public String State
        {
            get
            {
                return this.state;
            }

            set
            {
                this.state = value;
            }
        }

        public List<String> Target
        {
            get
            {
                return this.targets;
            }

            set
            {
                this.targets = value;
            }
        }
    }

}
