﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ODSClient.Model.Data
{
    public class Entity
    {
        private String entityId = null;
        private List<EntityInstance> instances = null;

        public String Id
        {
            get
            {
                return this.entityId;
            }

            set
            {
                this.entityId = value;
            }
        }

        public List<EntityInstance> Instances
        {
            get
            {
                return this.instances;
            }

            set
            {
                this.instances = value;
            }
        }
    }

    public class GlobalEntity : EntityInstance
    {


    }

    public class EntityInstance
    {
        private String instanceId = null;
        private List<LocalAttribute> attributes = null;
        private List<Entity> entities = null;
        private List<Relationship> relationships = null;

        public String Id
        {
            get
            {
                return this.instanceId;
            }

            set
            {
                this.instanceId = value;
            }
        }


        public List<LocalAttribute> Attributes
        {
            get
            {
                return this.attributes;
            }

            set
            {
                this.attributes = value;
            }
        }

        public List<Entity> Entities
        {
            get
            {
                return this.entities;
            }

            set
            {
                this.entities = value;
            }
        }

        public List<Relationship> Relationships
        {
            get
            {
                return this.relationships;
            }

            set
            {
                this.relationships = value;
            }
        }
    }
}
