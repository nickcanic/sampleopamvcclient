﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ODSClient.Model.Error
{
    public abstract class SampleApplicationException :
        Exception
    {
        protected String message = null;

        public override string Message
        {
            get
            {
                return this.message;
            }
        }

    }

    public class ServiceNotFoundException : SampleApplicationException
    {
        private String defaultMessage = "No service reference was found for rulebase: {0}";
        
        private String rulebase = null;

        public ServiceNotFoundException(String rulebase)
        {
            this.rulebase = rulebase;
        }

        public ServiceNotFoundException(String rulebase,String message)
        {
            this.rulebase = rulebase;
            this.message = message;
        }

        public override string Message
        {
            get
            {
                return (this.message==null)? String.Format(this.defaultMessage, new String[]{this.rulebase}) : this.message;
            }
        }
    }

    public class MissingConfigurationException : SampleApplicationException
    {
        public MissingConfigurationException(String message)
        {
            this.message = message;
        }
    }

    public class DataTypeError : ApplicationException
    {

    }
}
