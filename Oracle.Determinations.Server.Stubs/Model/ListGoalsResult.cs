﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ODSClient.Model
{
    public class ListGoalsResult
    {
        private List<ListGoalsEntity> entities = null;
        public ListGoalsResult(List<ListGoalsEntity> entities)
        {
            this.entities = entities;
        }

        public List<ListGoalsEntity> GetEntities()
        {
            return this.entities;
        }

    }

    public class ListGoalsEntity
    {
        private String entityName = null;
        private List<ListGoalsEntityInstance> instances = null;

        public ListGoalsEntity(String entityName, List<ListGoalsEntityInstance> instances)
        {
            EntityName = entityName;
            this.instances = instances;
        }

        public String EntityName
        {
            get
            {
                return this.entityName;
            }

            set
            {
                this.entityName = value;
            }
        }

        public List<ListGoalsEntityInstance> GetInstances()
        {
            return this.instances;
        }

    }

    public class ListGoalsEntityInstance
    {
        private String entityInstanceId = null;
        private List<Goal> goals = null;
        public ListGoalsEntityInstance(String instanceId,List<Goal> goals)
        {
            this.entityInstanceId = instanceId;
            this.goals = goals;

        }

        public String GetEntityInstanceId()
        {
            return this.entityInstanceId;
        }

        public List<Goal> GetGoals()
        {
            return this.goals;
        }

    }

    public class Goal
    {
        private String attributeId = null;
        private String goalId = null;
        private String goalText = null;
        private bool hasdecisionreport;

        public Goal(String attributeId, String goalId, String goalText, bool hasdecisionreport)
        {
            this.attributeId = attributeId;
            this.goalId = goalId;
            this.goalText = goalText;
            this.hasdecisionreport = hasdecisionreport;

        }

        public String GetAttributeId()
        {
            return attributeId;
        }

        public String GetGoalId()
        {
            return goalId;
        }

        public String GetGoalText()
        {
            return goalText;
        }

        public bool HasDecisionReport()
        {
            return hasdecisionreport;
        }
    }
}
