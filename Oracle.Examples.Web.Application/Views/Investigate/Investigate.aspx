﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>
<%@ Import Namespace="ODSClient.Model" %>
<%@ Import Namespace="ODSClient.Model.Screen" %>
<%@ Import Namespace="ODSClient.Model.Screen.Controls" %>
<%@ Import Namespace="Oracle.Examples.Web.Application.Utils" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
  Investigate Screen
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <form id="investigate_form"  action="Submit" runat="server">
        <% ClientInterviewScreen screen = (ClientInterviewScreen)ViewData["screen"]; %>
        <h2><%=screen.ScreenTitle %></h2>
        <%if (screen.Errors != null)
          { %>
           <div id="screen_errors" style="color:Red;">
                <p>The following errors were detected:</p>
                <ul>
                <%for (int i = 0; i < screen.Errors.Length; i++)
                  { %>
                  <li><%=screen.Errors[i]%></li>
                <%} %>
                </ul>
           </div>
        <%}%>
        <% List<ScreenControl> controls = screen.ScreenControls;
           controls.ForEach(delegate(ScreenControl control) {
              if (control.Visible){
                  %>
                  <%=ScreenUtil.GenerateHtml(control)%>
                  <%
                  if (control.ErrorMessage != null)
                  { %>
                      <div id="error" style="color:Red;"><%=control.ErrorMessage%></div>
                  <% }
                  %>
                  <br />
                  <%
              }
           }); %>
        <input type="hidden" id="investigate_flag" name="investigate_flag" value='<%= ViewData["investigate_flag"] %>' />
        <p><input id="submit_button" name="submit_button" type="submit" value="Next >" /></p>

    </form>

</asp:Content>
