﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>
<%@ Import Namespace="ODSClient.Model" %>
<%@ Import Namespace="Oracle.Examples.Web.Application.Utils" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Load Data
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Load Data</h2>
    <form id="load_case_form"  action="LoadData" runat="server">
        <h3>Select the case id from the list:</h3>
        <%  String[] caseIds = (String[])ViewData["case_id"]; 
            if (caseIds != null) {
                for (int i = 0; i < caseIds.Length; i++)
                {
                    %>
                    <input type="radio" id="caseId" name="caseId" value="<%=caseIds[i]%>" /><%=caseIds[i]%> <br />
                    <%
                }
            }%>
        <p>
            <input id="submit_button" name="submit_button" type="submit" value="OK" />
            <input id="submit_button" name="submit_button" type="submit" value="Cancel" />
        </p>

    </form>
</asp:Content>


