﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Oracle.Examples.Web.Application.Utils
{
    public class URLUtil
    {
        public static String BuildLink(String controller,
            String action, Dictionary<String,String> parameters) {
            String url = String.Format("/{0}/{1}", controller,action);
            if (parameters != null && parameters.Count > 0)
            {
                Dictionary<String,String>.KeyCollection.Enumerator keys = parameters.Keys.GetEnumerator();
                int count = 0;
                while (keys.MoveNext())
                {
                    String current = keys.Current;
                    String value = null;
                    if(parameters.TryGetValue(current, out value)) {
                        url = String.Format("{0}{1}{2}={3}",
                            url, count==0? "?":"&", current, value);
                    }
                    count++;
                }

            }
            return url;
        }
    }
}
