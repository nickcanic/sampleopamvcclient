﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>
<%@ Import Namespace="ODSClient.Model" %>
<%@ Import Namespace="ODSClient.Model.Screen" %>
<%@ Import Namespace="ODSClient.Model.Screen.Controls" %>
<%@ Import Namespace="Oracle.Examples.Web.Application.Utils" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	DecisionReport
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
    <% ClientInterviewScreen screen = (ClientInterviewScreen)ViewData["screen"]; %>
    <h2>Decision Report Screen</h2>
    <% if (screen.ScreenControls != null)
       {
           if (screen.ScreenControls[0] is ClientDecisionReportControl)
           {
               ClientDecisionReportControl drCtrl = (ClientDecisionReportControl)screen.ScreenControls[0];
               DecisionReportNodeControl arCtrl = drCtrl.AttributeControl;
              %>
              <%=ScreenUtil.GenerateDecisionReportTree(arCtrl)%>
              <%
           }
       }%>

    </form>

</asp:Content>




