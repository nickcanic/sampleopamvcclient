﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>
<%@ Import Namespace="ODSClient.Model" %>
<%@ Import Namespace="Oracle.Examples.Web.Application.Utils" %>
<%@ Import Namespace="ODSClient.Model.Data" %>
<%@ Import Namespace="ODSClient.Model.Screen" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Screens List
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Screens List</h2>
    <%List<ListScreensEntity> entities = (List<ListScreensEntity>)ViewData["entities"];
      entities.ForEach(delegate(ListScreensEntity entity)
      {
          %>
          <%=entity.GetId() %>
          <ul>
          <%
          List<ListScreensEntityInstance> instances = entity.Instances;
          instances.ForEach(delegate(ListScreensEntityInstance instance)
          {
              %>
              <li><%=instance.GetId() %></li>
              <ul>
              <%
              List<ClientInterviewScreen> screens = instance.Screens;
              screens.ForEach(delegate(ClientInterviewScreen screen)
              {
                  Dictionary<String,String> param = new Dictionary<string,string>();
                  param.Add("screenid", screen.ScreenId);
                  %>
                  <li><a href='<%=URLUtil.BuildLink("Investigate","GetScreen",param)%>'><%= screen.ScreenTitle%></a></li>
                  <%
              });
              %>
              </ul>
              <%
          });
          %>
          </ul>
          <%
      });
        
        %>
</asp:Content>



