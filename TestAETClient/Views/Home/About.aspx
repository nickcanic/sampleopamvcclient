﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="aboutTitle" ContentPlaceHolderID="TitleContent" runat="server">
    About Us
</asp:Content>

<asp:Content ID="aboutContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>About</h2>
    <p>
        This application's sole purpose is to test ODS Interview Service. 
    </p>
    <p>
        Web Application Target Framework: 3.5
    </p>
    <p>
        Web Service Client Target Framework: 2.0
    </p>
    <p>
        Programming Model: ASP.NET MVC
    </p>
    <br />

</asp:Content>
