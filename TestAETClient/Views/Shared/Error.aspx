﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<System.Web.Mvc.HandleErrorInfo>" %>
<%@ Import Namespace="Oracle.Examples.Web.Application.Utils" %>
<asp:Content ID="errorTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Error
</asp:Content>

<asp:Content ID="errorContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Sorry, an error occurred while processing your request.
    </h2>
    <p><%=ViewData["error_message"]%></p>
    <% String[] errors = (String[])ViewData["errors"];
       if (errors != null)
       {
           %>
           <ul>
           <% for (int i = 0; i < errors.Length; i++)
              {
                  %>
                  <li><%=errors[i] %></li>
                  <%
              }
            %>
           </ul>
           <%
       }
       %>
       
       
</asp:Content>
