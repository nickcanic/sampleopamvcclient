﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using ODSClient.Model;
using ODSClient.Model.Data;
using ODSClient.Model.Error;
using ODSClient.Model.Screen;
using ODSClient.Model.Screen.Controls;
using ODSClient.Service.Interview;
using Oracle.Examples.Web.Application.Utils;

namespace Oracle.Examples.Web.Application.Controllers.Interview
{
    public class InterviewController : ApplicationController
    {
        public override bool IsSessionRequired()
        {
            return true;
        }

        //
        // GET: /Summary/
        public ActionResult Start()
        {
            try
            {
                InitSession(this.Request.Params["rulebase"], this.Request.Params["locale"]);
            } catch(Exception ex) {
                ViewData["error_message"] = ex.Message;
                return View("Error");
            }
            return RedirectToAction("Summary");
        }

        public ActionResult Summary()
        {
            ActionResult result = CheckSession();
            if (result != null)
                return result;
            try
            {
                ViewData["screen"] = GetOrCreateSummaryScreen();
            }
            catch (Exception ex)
            {
                ViewData["error_message"] = ex.Message;
                return View("Error");
            }
            return View();
        }

        public ActionResult DecisionReport()
        {
            ActionResult result = CheckSession();
            if (result != null)
                return result;
            try
            {
                SessionData sessionData = GetSessionData();
                String goalState = this.Request.Params["goalstate"];
                InterviewService serviceProxy = ServiceLocator.GetServiceProxy(sessionData.RulebaseName);
                ViewData["screen"] = serviceProxy.GetDecisionReport(sessionData, goalState);
            }
            catch (Exception ex)
            {
                ViewData["error_message"] = ex.Message;
                return View("Error");
            }
            return View();
        }

        public ActionResult End()
        {
            SessionData sessionData = GetSessionData();
            String rulebase = sessionData.RulebaseName;
            String locale= sessionData.Locale;

            try
            {
                ClearSession();
                InitSession(rulebase, locale);
            }
            catch (Exception ex)
            {
                ViewData["error_message"] = ex.Message;
                return View("Error");
            }
            return RedirectToAction("Summary");
        }
        
        public ActionResult GetUserData()
        {
            ActionResult result = CheckSession();
            if (result != null)
                return result;
            
            try
            {
                SessionData sessionData = GetSessionData();
                InterviewService serviceProxy = ServiceLocator.GetServiceProxy(sessionData.RulebaseName);
                GlobalEntity global = serviceProxy.GetUserSetData(sessionData.SessionId);
                ViewData["global"] = global;
            }
            catch (Exception ex)
            {
                ViewData["error_message"] = ex.Message;
                return View("Error");
            }
            return View("UserData");
        }

        public ActionResult ListScreens()
        {
            ActionResult result = CheckSession();
            if (result != null)
                return result;

            try
            {
                SessionData sessionData = GetSessionData();
                InterviewService serviceProxy = ServiceLocator.GetServiceProxy(sessionData.RulebaseName);
                List<ListScreensEntity> entities = serviceProxy.ListScreens(sessionData.SessionId);
                ViewData["entities"] = entities;
            }
            catch (Exception ex)
            {
                ViewData["error_message"] = ex.Message;
                return View("Error");
            }
            return View("ScreensList");
        }

        public ActionResult GetDocument()
        {
            ActionResult result = CheckSession();
            if (result != null)
                return result;
            try
            {
                SessionData sessionData = GetSessionData();
                String documentId = Request.Params["documentId"];
                InterviewService serviceProxy = ServiceLocator.GetServiceProxy(sessionData.RulebaseName);

                String document = serviceProxy.GetDocument(sessionData.SessionId, documentId);
                sessionData.CurrentScreen = null;
                ViewData["document"] = document;
            }
            catch (Exception ex)
            {
                ViewData["error_message"] = ex.Message;
                return View("Error");
            }
            return View("Document");
        }



        #region private members

        private void InitSession(String rulebase, String locale)
        {
            //create SessionData
            String sessionID = null;
            InterviewService serviceProxy = ServiceLocator.GetServiceProxy(rulebase);
            sessionID = serviceProxy.OpenSession(locale);
            if (sessionID != null)
            {
                ViewData["session_id"] = sessionID;
                SessionData sessionData = new SessionData(sessionID);
                sessionData.RulebaseName = rulebase;
                sessionData.Locale = locale;
                SetSessionData(sessionData);
            }
            else
            {
                throw new ApplicationException("Failed to create new session.");
            }
                  
        }

        private List<ScreenControl> ListGoals(String rulebaseName, String sessionId)
        {
            InterviewService serviceProxy = ServiceLocator.GetServiceProxy(rulebaseName);
            return serviceProxy.ListGoals(sessionId);
        }

        private ClientInterviewScreen GetOrCreateSummaryScreen()
        {
            SessionData sessionData = GetSessionData();
            sessionData.CurrentScreen = null;

            InterviewService proxy = ServiceLocator.GetServiceProxy(sessionData.RulebaseName);
            ClientInterviewScreen summaryScreen = proxy.GetScreen(sessionData.SessionId, ApplicationConstants.SUMMARY_SCREEN_ID);
            if (summaryScreen == null || summaryScreen.ScreenControls == null)
            {
                summaryScreen = new ClientInterviewScreen();
                summaryScreen.ScreenId= ApplicationConstants.SUMMARY_SCREEN_ID;
                summaryScreen.ScreenType = ApplicationConstants.SUMMARY_SCREEN_TYPE;
                summaryScreen.ScreenTitle = ApplicationConstants.SUMMARY_SCREEN_TITLE_AUTO;
                List<ScreenControl> goals = ListGoals(sessionData.RulebaseName, sessionData.SessionId);
                if (goals.Count > 0 ) {
                    summaryScreen.ScreenControls = goals;
                }
            }
            return summaryScreen;
        }

        #endregion



    }
}
