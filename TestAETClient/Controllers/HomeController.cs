﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using ODSClient.Model;
using ODSClient.Service.Interview;
using Oracle.Examples.Web.Application.Utils;

namespace Oracle.Examples.Web.Application.Controllers
{
    [HandleError]
    public class HomeController : ApplicationController
    {
       
        public ActionResult Index()
        {
            try
            {
                if (IsSessionFound())
                {
                    ClearSession();
                }     
                GetVersionDetails();
                ServerInformation serverInfo = new ServerInformation();
                ViewData["rulebases"] = serverInfo.GetRulebases();
            }
            catch (Exception ex)
            {
                ViewData["error_message"] = ex.Message;
                return View("Error");
            }
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        #region private members
        private void GetVersionDetails()
        {
            if (Session[ApplicationConstants.DETERMINATIONS_SERVER_VERSION] == null)
            {
                ServerInformation serverInfo = new ServerInformation();
                ServerVersionDetails versionDetails = serverInfo.GetVersionDetails();
                Session[ApplicationConstants.DETERMINATIONS_SERVER_VERSION] = versionDetails.GetDeterminationsServerVersion();
                Session[ApplicationConstants.DETERMINATIONS_ENGINE_VERSION] = versionDetails.GetDeterminationsEngineVersion();
                Session[ApplicationConstants.INTERVIEW_ENGINE_VERSION] = versionDetails.GetInterviewEngineVersion();
            }
        }

        #endregion

        public override bool IsSessionRequired()
        {
            return false;
        }
    }
}
