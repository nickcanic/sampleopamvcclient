﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oracle.Examples.Web.Application.Utils
{
    public class ApplicationConstants
    {
        public const String DETERMINATIONS_SERVER_VERSION = "ds_version";
        public const String DETERMINATIONS_ENGINE_VERSION = "de_version";
        public const String INTERVIEW_ENGINE_VERSION = "ie_version";
        public const String SESSION_DATA = "SessionDataObj";
        public const String SUMMARY_SCREEN_ID = "qs$summary$global$global";
        public const String SUMMARY_SCREEN_TYPE = "summary";
        public const String SUMMARY_SCREEN_TITLE_AUTO = "Automatic Summary Screen";

    }
}
