﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ODSClient.Model.Data;
using ODSClient.Model.Screen.Controls;

namespace Oracle.Examples.Web.Application.Utils
{
    public class ScreenUtil
    {
        //generates a textbox
        public static String GenerateHtml(ScreenControl screenControl)
        {
            if (screenControl is InputScreenControl)
                return GenerateHtmlInput((InputScreenControl)screenControl);
            else if (screenControl is ReferenceControl)
                return GenerateHtmlReferenceControl((ReferenceControl)screenControl);
            else if (screenControl is ClientLabelControl)
                return GenerateHtmlLabel((ClientLabelControl)screenControl);
            return "";
        }

        public static String GenerateHtmlLabel(ClientLabelControl labelControl)
        {
            return "<label>"+labelControl.Caption+"</label>";
        }
        public static String GenerateHtmlInput(InputScreenControl inputControl)
        {
            String htmlString = "";
            if (inputControl != null)
            {
                if (inputControl.Mandatory)
                    htmlString += "<label style=\"color:Red;\">*</label>";
                htmlString += "<label>"+inputControl.Caption+"</label>&nbsp;";
                if (inputControl.Options == null)
                {
                    Object value = inputControl.Value;
                    if (value is Unknown)
                    {
                        if (!(inputControl.DefaultValue is Unknown))
                        {
                            value = inputControl.DefaultValue;
                        }
                        else
                        {
                            value = "";
                        }
                    }
                    else if (value is Uncertain)
                    {
                        value = "";
                    }
                    htmlString += "<input type=\"text\" id=\"" + inputControl.ControlId + "\" name=\"" +
                        inputControl.ControlId + "\" value=\"" + value + "\"/>";
                }
                else
                {
                    htmlString += "<select id=\"" + inputControl.ControlId + "\" name=\"" +
                        inputControl.ControlId + "\" >";
                    List<ListOption> options = inputControl.Options;
                    options.ForEach(delegate(ListOption option)
                    {
                        if (option.IsVisible())
                        {
                            String value = option.GetValue().ToString();

                            htmlString += "<option value=\""+value+"\" ";
                            if (value.Equals(inputControl.Value.ToString()))
                            {
                                htmlString += "selected";
                            }
                            htmlString += " >"+option.GetDisplayText();
                            htmlString += "</option>";
                        }
                    });
                    htmlString += "</select>";
                }

            }
            return htmlString;
        }


        public static String GenerateHtmlReferenceControl(ReferenceControl referenceControl)
        {
            String htmlString = "";
            if (referenceControl != null)
            {
                htmlString += "<label>" + referenceControl.Caption + "</label>&nbsp;";
                htmlString += "<select id=\"" + referenceControl.ControlId + "\" name=\"" +
                    referenceControl.ControlId + "\" >";
                List<ListOption> options = referenceControl.PossibleTarget;
                options.ForEach(delegate(ListOption option)
                {
                    if (option.IsVisible())
                    {
                        String value = option.GetValue().ToString();

                        htmlString += "<option value=\"" + value + "\" ";
                        if (value.Equals(referenceControl.TargetEntityInstanceId))
                        {
                            htmlString += "selected";
                        }
                        htmlString += " >" + option.GetDisplayText();
                        htmlString += "</option>";
                    }
                });
                htmlString += "</select>";
            }

            return htmlString;
        }

        public static String GenerateEntityCollectHtml(ScreenControl screenControl, String instanceId)
        {
            if (screenControl is InputScreenControl)
                return GenerateEntityCollectHtmlInput((InputScreenControl)screenControl, instanceId);
            else if (screenControl is ReferenceControl)
                return GenerateEntityCollectHtmlReferenceControl((ReferenceControl)screenControl, instanceId);
            else if (screenControl is ClientLabelControl)
                return GenerateHtmlLabel((ClientLabelControl)screenControl);
            return "";
        }

        public static String GenerateEntityCollectHtmlInput(InputScreenControl inputControl, String instanceId)
        {
            String htmlString = null;
            if (inputControl != null && inputControl.Visible)
            {
                if (inputControl.Mandatory)
                    htmlString += "<label style=\"color:Red;\">*</label>";
                String controlId = inputControl.ControlId + instanceId;
                htmlString += "<label>" + inputControl.Caption + "</label>&nbsp;";
                if (inputControl.Options == null)
                {
                    Object value = inputControl.Value;
                    if (value is Unknown)
                    {
                        if (!(inputControl.DefaultValue is Unknown))
                        {
                            value = inputControl.DefaultValue;
                        }
                        else
                        {
                            value = "";
                        }
                    }
                    else if (value is Uncertain)
                    {
                        value = "";
                    }
                    htmlString += "<input type=\"text\" id=\"" + controlId + "\" name=\"" +
                        controlId + "\" value=\"" + value + "\"/>";
                }
                else
                {
                    htmlString += "<select id=\"" + controlId + "\" name=\"" +
                        controlId + "\" >";
                    List<ListOption> options = inputControl.Options;
                    options.ForEach(delegate(ListOption option)
                    {
                        if (option.IsVisible())
                        {
                            Object value = option.GetValue();

                            htmlString += "<option value=\"" + value + "\" ";
                            if (value == inputControl.Value)
                            {
                                htmlString += "selected";
                            }
                            htmlString += " >" + option.GetDisplayText();
                            htmlString += "</option>";
                        }
                    });
                    htmlString += "</select>";
                }
            }

            return htmlString;
        }

        public static String GenerateEntityCollectHtmlReferenceControl(ReferenceControl inputControl, String instanceId)
        {
            String htmlString = null;
            if (inputControl != null && inputControl.Visible)
            {
                String controlId = inputControl.ControlId + instanceId;
                htmlString += "<label>" + inputControl.Caption + "</label>&nbsp;";
                htmlString += "<select id=\"" + controlId + "\" name=\"" +
                    controlId + "\" >";
                List<ListOption> options = inputControl.PossibleTarget;
                options.ForEach(delegate(ListOption option)
                {
                    if (option.IsVisible())
                    {
                        Object value = option.GetValue();

                        htmlString += "<option value=\"" + value + "\" ";
                        if (((String)value).Equals(inputControl.TargetEntityInstanceId))
                        {
                            htmlString += "selected";
                        }
                        htmlString += " >" + option.GetDisplayText();
                        htmlString += "</option>";
                    }
                });
                htmlString += "</select>";
            }

            return htmlString;
        }

        public static String GenerateDecisionReportTree(DecisionReportNodeControl node)
        {
            String htmlString = "<h3>"+ node.Caption +"</h3>" ;
            if (node.Controls != null && node.Controls.Count > 0)
            {
                htmlString += WriteChildren(node.Controls);
            }
            return htmlString;
        }

        private static String WriteChildren(List<ScreenControl> children)
        {
            String htmlString = "";
            htmlString += "<ul>";
            for (int i = 0; i < children.Count; i++)
            {
                DecisionReportNodeControl child = (DecisionReportNodeControl)children[i];
                if (child.Visible)
                {
                    htmlString += "<li>" + child.Caption;
                    if (child.DisplayValue != null)
                        htmlString += " - <b>" + child.DisplayValue + "</b>";
                    if (child.Controls != null && child.Controls.Count > 0)
                    {
                        htmlString += WriteChildren(child.Controls);
                    }
                }
            }

            htmlString += "</ul>";
            return htmlString;
        }

        public static String PrintUserData(GlobalEntity global)
        {
            String htmlString = "";
            htmlString+="<p>global:<b>"+global.Id+"</b></p>";
            htmlString += PrintAttributes(global.Attributes);
            htmlString += PrintEntities(global.Entities);
            return htmlString;
        }

        public static String PrintAttributes(List<LocalAttribute> attributes)
        {
            String htmlString = "";
            if (attributes.Count > 0)
            {
                htmlString += "<p>attributes:</p><ul>";
                attributes.ForEach(delegate(LocalAttribute attr)
                {
                    htmlString += "<li>" + attr.Id + ":<b>" + attr.Value + "</b>";
                });
                htmlString += "</ul>";
            }
            return htmlString;
        }

        public static String PrintEntities(List<Entity> entities)
        {
            String htmlString = "";
            if (entities.Count > 0)
            {
                htmlString += "<p>entities:</p><ul>";
                entities.ForEach(delegate(Entity entity)
                {
                    htmlString += "<li>entity:<b>" + entity.Id + "</b>";
                    htmlString += PrintEntityInstances(entity.Instances);
                });
                htmlString += "</ul>";
            }
            return htmlString;
        }

        public static String PrintEntityInstances(List<EntityInstance> instances)
        {
            String htmlString ="";
            if (instances.Count > 0)
            {
                htmlString += "<ul>";
                instances.ForEach(delegate(EntityInstance instance)
                {
                    htmlString += "<li>" + instance.Id;
                    htmlString += PrintAttributes(instance.Attributes);
                    htmlString += PrintEntities(instance.Entities);
                });
                htmlString += "</ul>";
            }
            return htmlString;
        }

        public static String PrintRelationships(List<Relationship> relationships)
        {
            String htmlString = "";
            if (relationships.Count > 0)
            {
                htmlString += "<p>entities:</p><ul>";
                relationships.ForEach(delegate(Relationship relationship)
                {
                    htmlString += "<li>relationship :<b>" + relationship.Id + "</b>";
                    if (relationship.Target.Count > 0)
                    {
                        htmlString += "<ul>targets :";
                        relationship.Target.ForEach(delegate(String target)
                        {
                            htmlString += "<li>" + target;
                        });
                        htmlString += "</ul>";
                    }
                });
                htmlString += "</ul>";
            }
            return htmlString;
        }
    }
}
